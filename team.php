<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    <section class="inner-banner-background">
        <img src="images/1920x534/aboutUs.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/aboutUsSmall.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/aboutUsMobile.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i> -->
            our team<br>
            <!-- <span>About Us</span> -->
        </p>
        <div class="bottom-line"></div>
    </div>
</div>
<div class="container padding-top-bottom team-member">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Manish-Gupta-chairman.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>MG</h6>
                <p>Chairman</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Rachna-Gupta-Director.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Rachna Gupta</h6>
                <p>Director</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Amit-Bhatta.jpg" class="img-responsive">
            <div class="team-ppl-box"s>
                <h6>Amit Bhatta</h6>
                <p>Business Head</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Pralhad--Baldawa-(-Manager-Accounts).jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Pralhad Baldawa</h6>
                <p>Accounts Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Pallavi-Shrivastava---Operations-Management.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Pallawi Nidhi</h6>
                <p>HR & Operations Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Preeti-Sharma---Digital-Marketing-&-Customer-Relationship.jpg" class="img-responsive">
            <div class="team-ppl-box" style="padding: 11px;">
                <h6>Preeti Sharma</h6>
                <p>Digital Marketing Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Chandresh-Mehta.jpg" class="img-responsive">
            <div class="team-ppl-box" style="padding: 11px;">
                <h6>Chandresh Mehta</h6>
                <p>Sales Head</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Vibha-Pandey.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Vibha Pandey</h6>
                <p>Review Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Priya-Joshi-(-Sr.-Sales-Manager).jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Priya Joshi</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Gurunath-Muley.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Gurunath Muley</h6>
                <p>Asst. Digital Marketing</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Gaurav-Navale.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Gaurav Navale</h6>
                <p>Accounts Executive</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Arjit-Garg.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Arjit Garg</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Kiran-Phalke.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Kiran Phalke</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Shvetank-Singh.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Shvetank Singh</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Jayesh-Japadia.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Jayesh Japadia</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Vidisha-Kukreja.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Vidisha Kukreja</h6>
                <p>Asst. Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Minal-Kale.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Minal Kale</h6>
                <p>CEF Coordinator</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Sandhya-P.V..jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Sandhya P.V.</h6>
                <p>EA to Director</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Bhavesh-Kumbhar.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Bhavesh Kumbhar</h6>
                <p>Graphics Designer</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Ashwini-Kadam.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Ashwini Kadam</h6>
                <p>Sales Coordinator</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Aanchal-Deshmukh.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Aanchal Deshmukh</h6>
                <p>Review Proffesional</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Preeti-Jeenwal.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Preeti Jeenwal</h6>
                <p>Sales Coordinator</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Apurva-Chandel.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Apurva Chandel</h6>
                <p>Asst. Manager Operations</p>
            </div>
        </div>
        <!-- <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Sakshi-Kartha.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Sakshi Kartha</h6>
                <p>Review Profesional</p>
            </div>
        </div> -->
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Manishankar-Vishwakarma.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Manishankar Vishwakarma</h6>
                <p>Office Executive</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Sunny-Pawar.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Sunny Pawar</h6>
                <p>Office Executive</p>
            </div>
        </div>
    </div>
</div>

<div class="container-full special-moments">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50">
                <h4 class="section-title">Team Chrysalis Special Moments</h4>
                <div class="bottom-line"></div>
                <p class="title-qoute margin-top-20 col-lg-8 col-md-8 col-sm-8 center-block">
                    At Chrysalis, we are a family of thinkers who work together to achieve the vision of company and driven with a strong purpose. Team Chrysalis lives all core values perfectly one of those core values are "Live, Learn, Love & Leave a Legacy".
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="row row-no-padding">
        <div class="gallery-slider">
            <a class="item" href="images/team/team-2.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-2-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-1.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-1-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-3.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-3-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-4.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-4-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-5.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-5-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-6.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-6-thumb.jpg" alt="title" />
            </a>
        </div>
    </div>
</div>  
<div class="container-full testimonial-slider testimonial-team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="slider">
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/preeti.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            My main motivation for joining Chrysalis revolves around the incredible growth potential for the company as well as Clients. Every day is full of exciting & new challenges and management provides me with the support to overcome any roadblocks. I think that little monitoring with the freedom to explore new thing with hard work is the key of my successful tenure with Chrysalis as a Digital Marketing Professional.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Preeti Sharma | Team Chrysalis</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/shraddha.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            Every day, I look forward to come here. We frequently share our best practices and concentrate on expanding our combined knowledge base. I value working with our customers and am thankful for the strong, supportive relationship I have been able to develop with our clients. In my position, I have the chance to think outside the box and identify new solutions while continuing to learn and grow in my role. 
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Shradha Todkar | Team Chrysalis</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/pallavi.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            From my first interaction with Chrysalis, I realized the company was fully committed to offering the highest level of service to its clients. Since joining, I've had the opportunity to participate in several company-wide initiatives to develop efficient processes in vendor management. Finally, Chrysalis’s dynamic environment provides a truly motivating workplace for me and my team. 
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Pallawi Shrivastava | Team Chrysalis</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>