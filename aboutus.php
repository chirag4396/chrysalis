<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
       
</div><br><br>


<div class="container-full padding-top-bottom">
    <div class="container our-programs">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <!-- <h4 class="section-title">we are chrysalis</h4> -->
                <p class="title-qoute center-block">
                    Chrysalis is into the business of "<strong>Empowering Entrepreneurs</strong>” through its core activities of business coaching, business consulting and organizational turnaround interventions.
                </p>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <p>Chrysalis was founded by MG in 1997 with a very powerful purpose which was "<strong>to enable individuals and organizations reach a higher level of thinking thereby giving a meaning to their existence.</strong>"</p>
                    <p>Under his visionary guidance and mentorship Chrysalis has played a pivotal role in the lives of millions of people comprising of students, house wives, working professionals, entrepreneurs and corporate companies. Chrysalis has been instrumental in providing a deeper meaning to their life and inspiring them to achieve much higher results and professional profitability.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <p>Chrysalis believes that success is a combination of five elements. To be successful, one needs to continuously expand ones knowledge, create strong attitudes, develop new skills, form good habits and implement result focused strategies.</p>
                    <!-- <p>The company has grown phenomenally since the time of inception in 1997 by using the same philosophy and is continuously moving towards its vision of "creating 1 million success stories by 2020."Today Chrysalis is a team of very efficient & dynamic people and is associated with a highly qualified communication, soft skills and entrepreneur coaches.</p> -->
                    <p>The company has grown phenomenally since the time of inception in 1997 by using the same philosophy and is continuously moving towards its vision of "creating 1 million success stories by 2020 and empowering 10,000 Entrepreneurs.</p>
                </div>

            </div>
        </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="programs-wrapper">
                <div class="col-lg-6 col-md-6 col-sm-12 no-padding purpose-img prog-height-fix">
                    <img class="img-responsive " src="images/ourPurpose.jpg">
                    <img class="img-responsive hidden-md hidden-sm hidden-lg" src="images/ourPurpose.jpg">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 about-us-our-purpose our-programs-bg">
                    <h5 class="sub-title">Our&nbsp;<span>Purpose</span></h5>
                    <p>To enable individuals and organisations reach a higher level of thinking, thereby giving a meaning to their existence</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
<div class="container-full">
    <div class="container">
    </div>
</div>



<!--<div class="container-full header-container our-vision padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-15">
                <h4 class="section-title">Our Vision</h4>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-1 col-md-1 col-sm-1">
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <p>By 2020 creating one million success stories and empowering<br>10,000 Entrepreneurs</p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                </div>
            </div>
        </div>
    </div>
</div>-->



<!-- services -->
<div class="w3ls-section services" id="services">
    <div class="container">
         <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50 myttl">
            <h4 class="section-title">Our Core Values</h4>
            <p class="title-qoute col-lg-8 col-md-8 col-sm-8 center-block">
               
            </p>
            <div class="bottom-line"></div>
        </div>
        <div class="services-w3ls-row agileits-w3layouts service1 text-center">
            <p> No free lunch<br>
                No entitlements<br>
                Merit based opportunities</p>  
            <div class="s-top">
                <span class="fa fa-cog" aria-hidden="true"></span>
            </div>

        </div>
        <div class="services-w3ls-row agileits-w3layouts middlegrid-w3ls">
            <div class="col-md-4 services-grid agileits-w3layouts service2">
                <div class="col-md-10 w3ls-sub-text">
                    <p>Organization before self<br>
                            We are solution oriented<br>
                        We have open communication</p>
                </div>  

                <div class="col-md-2 sub-icon">
                    <span class="fa fa-cog" aria-hidden="true"></span>
                </div>             
            </div>
            <div class="col-md-4 services-grid img-agileits">
                <img src="images/services.jpg" class="img-responsive" alt="" />
            </div>
            <div class="col-md-4 services-grid agileits-w3layouts service3">               
                
                <div class="col-md-2 sub-icon">
                    <span class="fa fa-cog" aria-hidden="true"></span>
                </div>

                <div class="col-md-10 w3ls-sub-text">
                     <p> Live, learn, love & leave a legacy<br>
                            We enjoy health & prosperity<br>
                            We have a learning culture<br>
                            We enrich relationships by giving back in abundance<br>
                        We contribute to humanity</p>                                   
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>

        <div class="services-w3ls-row agileits-w3layouts service4">
            <div class="s-top">
                <span class="fa fa-cog" aria-hidden="true"></span>
            </div>
            <p>We strive for excellence</p>
        </div>


    </div>
</div><br>
<!-- //services -->
<div class="clearfix"></div>







<div class="container-full header-container inner-header-container">   
    <div class="inner-banner-qoute2">
        <h4> our team </h4>
        <div class="bottom-line"></div>
    </div>
</div>

<div class="container padding-top-bottom team-member">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Manish-Gupta-chairman.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>MG</h6>
                    <p>Chairman</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Rachna-Gupta-Director.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Rachna Gupta</h6>
                    <p>Director</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Pralhad--Baldawa-(-Manager-Accounts).jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Pralhad Baldawa</h6>
                    <p>Accounts Manager</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Pallavi-Shrivastava---Operations-Management.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Pallawi Nidhi</h6>
                    <p>HR & Operations Manager</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Chandresh-Mehta.jpg" class="img-responsive">
                <div class="team-ppl-box" style="padding: 11px;">
                    <h6>Chandresh Mehta</h6>
                    <p>Sales Head</p>
                </div>
            </div>
        </div>
      
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Priya-Joshi-(-Sr.-Sales-Manager).jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Priya Joshi</h6>
                    <p>Manager Sales</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Gurunath-Muley.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Gurunath Muley</h6>
                    <p>Asst. Digital Marketing</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Arjit-Garg.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Arjit Garg</h6>
                    <p>Manager Sales</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Shvetank-Singh.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Shvetank Singh</h6>
                    <p>Manager Sales</p>            
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Jayesh-Japadia.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Jayesh Japadia</h6>
                    <p>Manager Sales</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Vidisha-Kukreja.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Vidisha Kukreja</h6>
                    <p>Asst. Manager Sales</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">    
                <img src="images/team/Sandhya-P.V..jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Sandhya P.V.</h6>
                    <p>EA to Director</p>
                </div>
            </div>
        </div>
       
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Ashwini-Kadam.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Ashwini Kadam</h6>
                    <p>Sales Coordinator</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Aanchal-Deshmukh.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Aanchal Deshmukh</h6>
                    <p>Review Proffesional</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Preeti-Jeenwal.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Preeti Jeenwal</h6>
                    <p>Sales Coordinator</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Apurva-Chandel.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Apurva Chandel</h6>
                    <p>Asst. Manager Operations</p>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Sakshi-Kartha.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Sakshi Kartha</h6>
                <p>Review Profesional</p>
            </div>
        </div> -->
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
            <div class="myteam-box">
                <img src="images/team/Manishankar-Vishwakarma.jpg" class="img-responsive">
                <div class="team-ppl-box">
                    <h6>Manishankar Vishwakarma</h6>
                    <p>Office Executive</p>
                </div>
            </div>
        </div>
       
    </div>
</div>

<div class="container-full special-moments" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50">
                <h4 class="section-title">Team Chrysalis Special Moments</h4>
                <div class="bottom-line"></div>
                <p class="title-qoute margin-top-20 col-lg-8 col-md-8 col-sm-8 center-block">
                    At Chrysalis, we are a family of thinkers who work together to achieve the vision of company and driven with a strong purpose. Team Chrysalis lives all core values perfectly one of those core values are "Live, Learn, Love & Leave a Legacy".
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container-full"  style="display: none;">
    <div class="row row-no-padding">
        <div class="gallery-slider">
            <a class="item" href="images/team/team-2.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-2-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-1.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-1-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-3.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-3-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-4.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-4-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-5.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-5-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-6.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-6-thumb.jpg" alt="title" />
            </a>
        </div>
    </div>
</div>
</div>
<div class="container-full testimonial-slider testimonial-team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="slider">
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/preeti.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            My main motivation for joining Chrysalis revolves around the incredible growth potential for the company as well as Clients. Every day is full of exciting & new challenges and management provides me with the support to overcome any roadblocks. I think that little monitoring with the freedom to explore new thing with hard work is the key of my successful tenure with Chrysalis as a Digital Marketing Professional.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Preeti Sharma | Team Chrysalis</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/shraddha.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>

                            Every day, I look forward to come here. We frequently share our best practices and concentrate on expanding our combined knowledge base. I value working with our customers and am thankful for the strong, supportive relationship I have been able to develop with our clients. In my position, I have the chance to think outside the box and identify new solutions while continuing to learn and grow in my role.

                            Every day, I look forward to come here. We frequently share our best practices and concentrate on expanding our combined knowledge base. I value working with our customers and am thankful for the strong, supportive relationship I have been able to develop with our clients. In my position, I have the chance to think outside the box and identify new solutions while continuing to learn and grow in my role.

                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Shradha Todkar | Team Chrysalis</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/pallavi.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>

                            From my first interaction with Chrysalis, I realized the company was fully committed to offering the highest level of service to its clients. Since joining, I've had the opportunity to participate in several company-wide initiatives to develop efficient processes in vendor management. Finally, Chrysalis’s dynamic environment provides a truly motivating workplace for me and my team.

                            From my first interaction with Chrysalis, I realized the company was fully committed to offering the highest level of service to its clients. Since joining, I've had the opportunity to participate in several company-wide initiatives to develop efficient processes in vendor management. Finally, Chrysalis’s dynamic environment provides a truly motivating workplace for me and my team.

                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Pallawi Shrivastava | Team Chrysalis</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- -------------------------------TCF-------------------------------- -->




<div class="container-full padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="">
                    <div class="col-lg-6 col-md-6 col-sm-12 cf-logo">
                        <img src="images/cfLogo.png" class="img-responsive">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 cf-details">
                        <div class="">
                            <p><strong>TCF</strong> is the corporate social responsibility division of Chrysalis with the strong purpose “to contribute through our mind, body and soul and thereby create an empowered India.”
                            The Foundation has worked towards making a difference in the lives of people in India. It has encouraged people towards a more positive outlook, to take the responsibility to better their lives and the society.</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <br><center>
        <a href="tef.php" class="btn btn-info">Read More</a>
    </center>
</div>
</div>
<?php include 'footer.php';?>