<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>


</div>
<div class="container-full" style="margin-top: 5em;">
    <div class="container our-programs">
        <div class="contact_tab text-center">
            <ul id="myTab" class="container text-center nav nav-tabs" role="tablist">
                <li class="active"><a href="#tab1" role="tab" data-toggle="tab">About Chrysalis</a></li>
                <li><a href="#tab2" role="tab" data-toggle="tab">Team Chrysalis</a></li>
                <li><a href="#tab3" role="tab" data-toggle="tab">TCF</a></li>
                <li><a href="#tab4" role="tab" data-toggle="tab">CEF</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="tab1">
                    <div class="container">
                    <p>Eget pede fringilla rutrum. Fusce eget tortor vel magna iaculis convallis. Mauris molestie magna. Mauris  Integer eget arcu. asiscing eleifend dui. Nullam lectus neque,                                            blandit quis, mattis quis, varius eros. Proin porta pellentesque ante.Fusce In sagittis eros aliquam quis purus nunc ut turpis sed </p>
                        <div id="message"></div>
                        <form id="contactform" action="contact.php" name="contactform" method="post" data-scroll-reveal="enter from the bottom after 0.4s">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <input type="text" name="email" id="email" class="form-control" placeholder="Email Address">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <textarea class="form-control" name="comments" id="comments" rows="6" placeholder="Message"></textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" value="SEND" id="submit" class="btn btn-lg btn-contact btn-primary">SUBMIT</button>
                            </div>
                        </form> <!-- End Form -->
                    </div> <!-- End Container -->
                </div><!-- End Tab Pane -->

                        <!-- /Google Map -->
                <div class="tab-pane fade" id="tab2">
                    <div id="map"></div>
                </div>
            </div><!-- /end my tab content -->
        </div><!-- /contact_tab -->















        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <!-- <h4 class="section-title">we are chrysalis</h4> -->
                <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                    Chrysalis is into the business of "<strong>Empowering Entrepreneurs</strong>” through its core activities of business coaching, business consulting and organizational turnaround interventions.
                </p>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>Chrysalis was founded by MG in 1997 with a very powerful purpose which was "<strong>to enable individuals and organizations reach a higher level of thinking thereby giving a meaning to their existence.</strong>"</p>
                        <p>Under his visionary guidance and mentorship Chrysalis has played a pivotal role in the lives of millions of people comprising of students, house wives, working professionals, entrepreneurs and corporate companies. Chrysalis has been instrumental in providing a deeper meaning to their life and inspiring them to achieve much higher results and professional profitability.</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>Chrysalis believes that success is a combination of five elements. To be successful, one needs to continuously expand ones knowledge, create strong attitudes, develop new skills, form good habits and implement result focused strategies.</p>
                        <!-- <p>The company has grown phenomenally since the time of inception in 1997 by using the same philosophy and is continuously moving towards its vision of "creating 1 million success stories by 2020."Today Chrysalis is a team of very efficient & dynamic people and is associated with a highly qualified communication, soft skills and entrepreneur coaches.</p> -->
                        <p>The company has grown phenomenally since the time of inception in 1997 by using the same philosophy and is continuously moving towards its vision of "creating 1 million success stories by 2020 and empowering 10,000 Entrepreneurs.</p>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="programs-wrapper">
                    <div class="col-lg-6 col-md-6 col-sm-12 no-padding purpose-img prog-height-fix">
                        <img class="img-responsive " src="images/aboutPurpose.jpg">
                        <img class="img-responsive hidden-md hidden-sm hidden-lg" src="images/aboutPurpose_mobile.jpg">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 about-us-our-purpose our-programs-bg">
                        <h5 class="sub-title">Our&nbsp;<span>Purpose</span></h5>
                        <p>To enable individuals and organisations reach a higher level of thinking, thereby giving a meaning to their existence</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="container">
    </div>
</div>
<div class="container-full core-value-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50">
                <h4 class="section-title">Our Core Values</h4>
                <p class="title-qoute col-lg-8 col-md-8 col-sm-8 center-block">
                    Always strive for better work.
                </p>
                <div class="bottom-line"></div>
            </div>
            <div class="col-lg-12 core-value margin-bottom-50">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p> No free lunch<br>
                        No entitlements<br>
                        Merit based opportunities</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p>Organization before self<br>
                        We are solution oriented<br>
                        We have open communication</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p> Live, learn, love & leave a legacy<br>
                        We enjoy health & prosperity<br>
                        We have a learning culture<br>
                        We enrich relationships by giving back in abundance<br>
                        We contribute to humanity</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 last-value">
                        <i class="fa fa-star"></i>
                        <p>We strive for excellence</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-full header-container our-vision padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-15">
                <h4 class="section-title">Our Vision</h4>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <!-- <img src="" class="img-responsive"> -->
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <p>By 2020 creating one million success stories and empowering<br>10,000 Entrepreneurs</p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <!-- <img src="" class="img-responsive"> -->
                </div>

            </div>
        </div>
    </div>
</div>
<!-- --------------------------------------- -->
<div class="container-full header-container inner-header-container">

    <section class="inner-banner-background">
        <img src="images/1920x534/aboutUs.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/aboutUsSmall.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/aboutUsMobile.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i> -->
            our team<br>
            <!-- <span>About Us</span> -->
        </p>
        <div class="bottom-line"></div>
    </div>
</div>
<div class="container padding-top-bottom team-member">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Manish-Gupta-chairman.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>MG</h6>
                <p>Chairman</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Rachna-Gupta-Director.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Rachna Gupta</h6>
                <p>Director</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Amit-Bhatta.jpg" class="img-responsive">
            <div class="team-ppl-box"s>
                <h6>Amit Bhatta</h6>
                <p>Business Head</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Pralhad--Baldawa-(-Manager-Accounts).jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Pralhad Baldawa</h6>
                <p>Accounts Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Pallavi-Shrivastava---Operations-Management.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Pallawi Nidhi</h6>
                <p>HR & Operations Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Preeti-Sharma---Digital-Marketing-&-Customer-Relationship.jpg" class="img-responsive">
            <div class="team-ppl-box" style="padding: 11px;">
                <h6>Preeti Sharma</h6>
                <p>Digital Marketing Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Chandresh-Mehta.jpg" class="img-responsive">
            <div class="team-ppl-box" style="padding: 11px;">
                <h6>Chandresh Mehta</h6>
                <p>Sales Head</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Vibha-Pandey.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Vibha Pandey</h6>
                <p>Review Manager</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Priya-Joshi-(-Sr.-Sales-Manager).jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Priya Joshi</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Gurunath-Muley.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Gurunath Muley</h6>
                <p>Asst. Digital Marketing</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Gaurav-Navale.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Gaurav Navale</h6>
                <p>Accounts Executive</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Arjit-Garg.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Arjit Garg</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Kiran-Phalke.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Kiran Phalke</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Shvetank-Singh.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Shvetank Singh</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Jayesh-Japadia.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Jayesh Japadia</h6>
                <p>Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Vidisha-Kukreja.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Vidisha Kukreja</h6>
                <p>Asst. Manager Sales</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Minal-Kale.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Minal Kale</h6>
                <p>CEF Coordinator</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Sandhya-P.V..jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Sandhya P.V.</h6>
                <p>EA to Director</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Bhavesh-Kumbhar.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Bhavesh Kumbhar</h6>
                <p>Graphics Designer</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Ashwini-Kadam.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Ashwini Kadam</h6>
                <p>Sales Coordinator</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Aanchal-Deshmukh.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Aanchal Deshmukh</h6>
                <p>Review Proffesional</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Preeti-Jeenwal.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Preeti Jeenwal</h6>
                <p>Sales Coordinator</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Apurva-Chandel.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Apurva Chandel</h6>
                <p>Asst. Manager Operations</p>
            </div>
        </div>
        <!-- <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Sakshi-Kartha.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Sakshi Kartha</h6>
                <p>Review Profesional</p>
            </div>
        </div> -->
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Manishankar-Vishwakarma.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Manishankar Vishwakarma</h6>
                <p>Office Executive</p>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 margin-bottom-40">
                <img src="images/team/Sunny-Pawar.jpg" class="img-responsive">
            <div class="team-ppl-box">
                <h6>Sunny Pawar</h6>
                <p>Office Executive</p>
            </div>
        </div>
    </div>
</div>

<div class="container-full special-moments">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50">
                <h4 class="section-title">Team Chrysalis Special Moments</h4>
                <div class="bottom-line"></div>
                <p class="title-qoute margin-top-20 col-lg-8 col-md-8 col-sm-8 center-block">
                    At Chrysalis, we are a family of thinkers who work together to achieve the vision of company and driven with a strong purpose. Team Chrysalis lives all core values perfectly one of those core values are "Live, Learn, Love & Leave a Legacy".
                </p>
            </div>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="row row-no-padding">
        <div class="gallery-slider">
            <a class="item" href="images/team/team-2.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-2-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-1.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-1-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-3.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-3-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-4.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-4-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-5.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-5-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/team/team-6.jpg" rel="prettyPhoto[staff]">
                <img class="item img-responsive" src="images/team/team-6-thumb.jpg" alt="title" />
            </a>
        </div>
    </div>
</div>
<div class="container-full testimonial-slider testimonial-team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="slider">
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/preeti.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            My main motivation for joining Chrysalis revolves around the incredible growth potential for the company as well as Clients. Every day is full of exciting & new challenges and management provides me with the support to overcome any roadblocks. I think that little monitoring with the freedom to explore new thing with hard work is the key of my successful tenure with Chrysalis as a Digital Marketing Professional.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Preeti Sharma | Team Chrysalis</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/shraddha.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            Every day, I look forward to come here. We frequently share our best practices and concentrate on expanding our combined knowledge base. I value working with our customers and am thankful for the strong, supportive relationship I have been able to develop with our clients. In my position, I have the chance to think outside the box and identify new solutions while continuing to learn and grow in my role.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Shradha Todkar | Team Chrysalis</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/team/pallavi.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            From my first interaction with Chrysalis, I realized the company was fully committed to offering the highest level of service to its clients. Since joining, I've had the opportunity to participate in several company-wide initiatives to develop efficient processes in vendor management. Finally, Chrysalis’s dynamic environment provides a truly motivating workplace for me and my team.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Pallawi Shrivastava | Team Chrysalis</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- -------------------------------TCF-------------------------------- -->


<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    <section class="inner-banner-background">
        <img src="images/1920x534/about.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/about.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/about.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i> -->
            The Chrysalis Foundation <br>
        </p>
        <div class="bottom-line"></div>
    </div>
</div>

<div class="container-full padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="">
                    <div class="col-lg-6 col-md-6 col-sm-12 cf-logo">
                        <img src="images/cfLogo.png" class="img-responsive">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 cf-details">
                        <div class="">
                            <p><strong>TCF</strong> is the corporate social responsibility division of Chrysalis with the strong purpose “to contribute through our mind, body and soul and thereby create an empowered India.”
                            The Foundation has worked towards making a difference in the lives of people in India. It has encouraged people towards a more positive outlook, to take the responsibility to better their lives and the society.</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="container initiatives">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-20">
                <h4 class="section-title">Initiatives</h4>
                <div class="bottom-line"></div>
            </div>
            <br class="clearfix">
            <div class="col-lg-12 margin-bottom-50">
                <ul>
                    <div class="circle top"></div>
                    <div class="circle bottom"></div>
                    <li>
                        <div class="data">
                            <div class="date"><span>From</span>2000</div>
                            <div class="initiative-img">
                                <img src="images/initiative-01.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Association with Army Paraplegic Rehabilitation Centre , khadki, Pune</h5>
                                <p>APRC is the centre for rehabilitation of paraplegic and tetra-plegic soldiers who have been injured on the war front while serving the nation.TCF has  been associated with APRC  since 1999. TCF has been instrumental in generating funds for APRC and has been an integral part of the lives of inmates by being with them for Independence Day, Republic Day and  Ganesh Chaturthi celebrations.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2010</div>
                            <div class="initiative-img">
                                <img src="images/initiative-02.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Association with Manavya</h5>
                                <p>Manavya is a home for nursing, caring and sheltering orphaned and destitute children and women afflicted by HIV/AIDS. The foundation raised funds for the organisation by organizing power talks titled “I – The Miracle”.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2010</div>
                            <div class="initiative-img">
                                <img src="images/initiative-03.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Roti, Kapda, Muskan<br>
                                (An initiative to collect rice and clothes for the tribals of Velhe)
                                </h5>
                                <p>Manavya is a home for nursing, caring and sheltering orphaned and destitute children and women afflicted by HIV/AIDS. The foundation raised funds for the organisation by organizing power talks titled “I – The Miracle”.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2012</div>
                            <div class="initiative-img">
                                <img src="images/initiative-04.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Mission Jal Samruddhi <br>
                                (Initiative to desilt a 100-acre lake to provide potable water in Beed)
                                </h5>
                                <p>TCF initiated “Project Jal Samruddhi” to desilt the 100 acre Nizam Era Twarita Devi Pond at Talawade in Beed District by raising funds to the tune of well over Rs. 30 Lakhs for this project. This project resulted in 1500 acres of land becoming fertile and 60 crore liters of water generation for the area.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2013</div>
                            <div class="initiative-img">
                                <img src="images/initiative-05.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>One Day for My City</h5>
                                <p>It is an initiative to encourage the citizens of the city to give one day periodically for the betterment of the city. Some of the activities undertaken under this initiative are cleaning of PMPML buses, Creating awareness among citizens to participate in the electoral process by exercising their right to vote in the Recent Lok Sabha Elections and participating in the Swachh Bharat Abhiyan.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- ----------------------CEF----------------------------- -->

<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    <section class="inner-banner-background">
        <img src="images/1920x534/about.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/about.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/about.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i> -->
            Chrysalis Entrepreneur Forum<br>
        </p>
        <div class="bottom-line"></div>
    </div>
</div>
<div class="container-full">
    <div class="container our-programs cef-w-a-chry">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <!-- <h4 class="section-title">Chrysalis Entrepreneur Forum</h4> -->
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>CEF is a conglomerate of Entrepreneurs which aims to build better businesses and in turn make a positive dent in the GDP of The Nation. It maintains the momentum that is built during "The Game of Business" an entrepreneur empowerment program. The forum consists of core members who provide the growth ecosystem to the </p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>different teams of CEF members from different industries, who are ambitious, growth oriented and committed to learn and share from their live experiences and together achieve growth. Single intelligence is always subordinated by cumulative or group intelligence.</p>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="cef-our-purpose padding-top-bottom">
                    <div class="col-lg-6 col-md-6 col-sm-12 pull-right">
                        <h5 class="sub-title">Our&nbsp;<span>Purpose</span></h5>
                        <p>To continuously experience the joy of building<br> better businesses, thereby leading to inclusive<br> growth.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full core-value-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50">
                <h4 class="section-title">Our Core Values</h4>
                <div class="bottom-line"></div>
            </div>
            <div class="col-lg-12 core-value margin-bottom-50">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p>We will Build Success in<br>Others: Success transfers</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p>Abundance Mindset<br><br></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p>Implementation</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 last-value">
                        <i class="fa fa-star"></i>
                        <p>No Postponement of Life</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full header-container our-vision cef-our-vision padding-top-bottom">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-15">
                <h4 class="section-title">Our Vision</h4>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <img src="" class="img-responsive">
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <p>Building a strong fraternity of growth hungry entrepreneurs supporting an<br> inbuilt ecosystem. We intend to make a contribution of 2% to the GDP<br> growth of India by 2025.</p>
                    <div class="bottom-line"></div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <img src="" class="img-responsive">
                </div>

            </div>
        </div>
    </div>
</div>
<div class="container-full cefActivitiesInitiatives padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4 class="section-title margin-bottom-40">CEF Activities &amp; Initiatives</h4>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <p>Apart from continuously upgrading oneself with organizational skills, various life enhancement activities & initiatives are taken in the domain of social, professional and a belief that there is no postponement of life. It is about the right balance between doing work and having fun.</p>
                    </div>
                    <div class="col-sm-6">
                        <strong>Speakers at CEF</strong>
                        <p>CEF invites eminent speakers every month from different walks of life to address the forum members which enables them to scale up their organisations.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full padding-top-bottom">
    <div class="container team-hum">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4 class="section-title">Team HUM</h4>
                <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                    An Organisational Vision Program
                </p>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="team-hum-desc">A 3 hour power packed session by MG that brings together Entrepreneurs and their team members to realign themselves to the organisational vision and purpose. In this program, MG connects seamlessly with every team member of various organizations and explains the sincere purpose of having a single vision in a context easily understood. The accent here is on how there should be team ownership and accountability by inculcating the core purpose and the core values of the organization.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



<?php include 'footer.php';?>