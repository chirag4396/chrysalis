<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 padding-top-bottom">
                <!-- <h4 class="section-title">Business Offerings</h4>
                <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                    Programs For Entrepreneurs And Their Teams
                </p> -->


                <div class="contact_tab myspace">
                    <ul id="myTab" class="container text-center nav nav-tabs" role="tablist">
                        <li class="active" id = "tgob-li"><a href="#tgob" role="tab" data-toggle="tab">Videos on Life</a></li>
                        <li id = "pankh-li"><a href="#pankh" role="tab" data-toggle="tab">Videos on Entrepreneurship and Leadership</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="tgob">
                            <div class="program-info">
                                <h4> </h4>  
                                <!--projects-->
                              <div class="gallery" id="projects">
                                  <div class="filtr-container">  

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/7U_ZT9K0uco" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/BwCH04pZ9Rc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/7U_ZT9K0uco" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>
                                       
                                      <div class="clearfix"> </div>
                                  </div>
                              </div>                                 
                            </div>
                        </div><!-- End Tab Pane -->

                        <div class="tab-pane fade" id="pankh">
                            <div class="program-info">
                                <h4></h4> 
                            </div>

                            <div class="gallery" id="projects">
                                  <div class="filtr-container">  
                                      <div class=" filtr-item" data-category="2" data-sort="Luminous night">
                                          <a href="images/event1.JPG" class="b-link-stripe b-animate-go  thickbox">
                                            <figure>
                                                <img src="images/event1.JPG" class="img-responsive" alt=" " />  <figcaption>
                                                <h3>Belas Creation</h3>
                                                <p>
                                                  We are created to create
                                                </p>
                                              </figcaption>
                                            </figure>
                                          </a>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/7U_ZT9K0uco" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/BwCH04pZ9Rc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/7U_ZT9K0uco" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>                                       
                                      <div class="clearfix"> </div>
                                  </div>
                              </div>     
                        </div>
                    </div>

                </div><!-- /end my tab content -->
            </div><!-- /contact_tab -->


        </div>
    </div>
</div>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">


            <div class="container">
                <div class="row">

                    <!-- Form -->
                    <div class="nb-form">
                        <p class="title text-center">Send Enquiry</p>   

                        <form method="POST" role="form" id="myform">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="" placeholder="Enter Full Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="" placeholder="Enter Email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" id="" placeholder="Enter Phone No.">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="inputMessage" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <center>
                                <input type="submit" name="name" id="submit" value="Submit" class="btn btn-default">
                            </center>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uprog-lnk">
                <p>To know about our upcoming program <a href="<?php echo $eventPath; ?>events-list/">click here</a></p>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php';?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $('#result').hide();
    });
    $("#submit").on('click',function(p){
        p.preventDefault();
        var formdata = new FormData($('#myform')[0]);

        $.ajax({
            url:'enquiry.php',
            type:'post',
            data:formdata,
            processData:false,
            contentType:false,
            success:function(res){
                if (res.indexOf('ok')>=0)
                {
                    $('#result').show();
                    $('#myform')[0].reset();
                    window.setTimeout(function(){
                        $('#result').hide('blind');
                    },3000);
                }
            }
        });
    });
    var menu = ['tgob', 'pankh', 'tl', 'nvision', 'alygn'];
    $('#menuTab li a').each(function(k,v){        
        $(v).on({
            'click' : function(){
                var id = $(this).attr('data-href');                
                $.each(menu, function(i,j){
                    if(id != j){
                        $('#'+j).removeClass('active in');
                        $('#'+j+'-li').removeClass('active');                                        
                    }
                });
                $('#'+id).addClass('active in');
                $('#'+id+'-li').addClass('active');                
            }
        });
    });
</script>
