
<!DOCTYPE html>
<head>
	<title>Chrysalis</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
<!-- <script src="js/jquery2.0.3.min.js"></script>
	<-->

	<script type="text/javascript" src="../sungare_admin/js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>

	<script>
		$(function () {
			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

			if (!screenfull.enabled) {
				return false;
			}

			$('#toggle').click(function () {
				screenfull.toggle($('#container')[0]);
			});	
		});
	</script>


	<!-- tables -->
	<link rel="stylesheet" type="text/css" href="css/table-style.css" />
	<link rel="stylesheet" type="text/css" href="css/basictable.css" />
	<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').basictable();

			$('#table-breakpoint').basictable({
				breakpoint: 768
			});

			$('#table-swap-axis').basictable({
				swapAxis: true
			});

			$('#table-force-off').basictable({
				forceResponsive: false
			});

			$('#table-no-resize').basictable({
				noResize: true
			});

			$('#table-two-axis').basictable();

			$('#table-max-height').basictable({
				tableWrapper: true
			});
		});
	</script>
	<!-- //tables -->
</head>
<body class="dashboard-page">
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>

		<div class="main-grid">
			<div class="agile-grids">	
				<!-- input-forms -->
				<div class="grids">
					<div class="progressbar-heading grids-heading">
						<h2>Testimonial</h2>
					</div>
					<div class="panel panel-widget forms-panel">
						<div class="forms">
							<div class="form-grids widget-shadow" data-example-id="basic-forms"> 
								<div class="form-title">
									<h4>Testimonial Form:</h4>
								</div>
								<div class="form-body">
									
									

									<form action="#" name="frmtest" id="frmtest" method="post" enctype="multipart/form-data"> 
										<?php 
										include("../config.php");
										$test_id=$_POST['test_id'];

										$res = $conn->query("select * from testimonial where test_id='$_POST[test_id]'");	
										if($res->num_rows){	
											while ($row = $res->fetch_assoc())
											{

												?>
												<div class="form-group"> 

													<label for="exampleInputEmail1" style="display: none;">Test Id</label> 
													<input type="text" name="test_id" class="form-control" id="test_id" placeholder="Enter  Name" style="display: none;" value="<?php echo $row['test_id']; ?>" > 
												</div>
												<div class="form-group"> 

													<label for="exampleInputEmail1">Name</label> 
													<input type="text" name="test_name" class="form-control" id="test_name" placeholder="Enter  Name" value="<?php echo $row['test_name']; ?>" > 
												</div>
												<div class="form-group"> 

													<label for="exampleInputEmail1">Company Name</label> 
													<input type="text" name="test_company_name" class="form-control" id="test_company_name" placeholder="Enter Company Name" value="<?php echo $row['test_company_name']; ?>"  > 
												</div>
												<div class="form-group"> 
													<label for="selector1">Select Category</label>
													<input type="hidden" name="test_category" id = "cate">
													<select id="test_category" class="form-control">
														
														<?php
														include "../config.php";
														$sql = "select * from categories"; 
														$qry = $conn->query($sql);
														while ($row=$qry->fetch_assoc()) 
														{

															echo '<option value="'.$row['test_cat_id'].'#'.preg_replace('/\s/','-',$row['test_cat_name']).'">'.$row['test_cat_name'].'</option>';
														}

														?>
														<option><?php echo $row['test_cat_name'] ?></option>

													</select>

												</div>
												
												<div class="form-group"> 

													<label for="exampleInputEmail1">Message</label> 
													<input type="text" name="test_msg" class="form-control" style="height: 150px;" id="test_msg"  value="<?php echo $row['test_msg']; ?>" > 

												</div>
												<!-- <div class="form-group"> 

													<label for="exampleInputEmail1">Video Link</label> 
													<input type="text" name="link" class="form-control"  id="link"  value="<?php echo $row['test_link']; ?>" > 

												</div> -->
												

												
												<div class="form-group"> 
													<label for="exampleInputFile">File input</label> 
													<input type="file" name="image" id="image" value="<?php echo 
													$row['test_image'];?>"><?php echo '<img src="'.$row['test_image'].'" width="100" class="img-thumbnail"/>'; ?> 

												<!-- <div class="form-group"> 

												<div id="dvPassport"  style="display: none">
													Video Link:
													<input type="text" class="form-control" id="vi" value="<?php echo $row['test_link']; ?>" name = "link" />
												</div>
											</div> -->



												<!-- <div class="form-group"> 
													<label for="exampleInputFile">Video Link</label> 
													<input type="text" name="link" id="link" value="<?php echo 
													$row['quote_video_link'];?>"><?php echo '<img src="'.$row['quote_video_link'].'" width="100" class="img-thumbnail"/>'; ?> 
>>>>>>> origin
													<p class="help-block">Example block-level help text here.</p> 

												</div>  -->
												<center><font color="black"><div id="success_message" style="display:none;">Data Updated Sucessfully </div></font></center>
												<button type="submit" name="btnup" id="btnup" class="btn btn-primary w3ls-button">Update</button> 

											</form> 

										</div>


									</div>
								</div>
							</div>

						</div>		
						<?php } }?>
					</div>
				</div>
				<script src="../sungare_admin/js/updatetestimonial.js"></script>

				<!-- footer -->
				<?php require("footer.php") ?>
				<!-- //footer -->
			</section>
<!-- 	<script src="js/bootstrap.js"></script>
-->	

</body>
</html>

