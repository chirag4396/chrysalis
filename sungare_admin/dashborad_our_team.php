
<!DOCTYPE html>
<head>
	<title>Chrysalis</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 

	<!-- //font-awesome icons -->
<!-- <script src="js/jquery2.0.3.min.js"></script>
	<-->
	<script type="text/javascript" src="../sungare_admin/js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/screenfull.js"></script>
	
<!-- 	<script type="text/javascript" src="../admin/js/checkbox.js"></script>
-->
<script>
	$(function () {
		$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

		if (!screenfull.enabled) {
			return false;
		}

		$('#toggle').click(function () {
			screenfull.toggle($('#container')[0]);
		});	
	});
</script>


<!-- tables -->
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<!-- <link rel="stylesheet" type="text/css" href="css/datatable.css" />
 -->
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').basictable();

		$('#table-breakpoint').basictable({
			breakpoint: 768
		});

		$('#table-swap-axis').basictable({
			swapAxis: true
		});

		$('#table-force-off').basictable({
			forceResponsive: false
		});

		$('#table-no-resize').basictable({
			noResize: true
		});

		$('#table-two-axis').basictable();

		$('#table-max-height').basictable({
			tableWrapper: true
		});
	});
</script>

<!-- //tables -->
</head>
<body class="dashboard-page">
	<?php require("nav_menu.php"); ?>

	<section class="wrapper scrollable">
		<nav class="user-menu">
			<a href="javascript:;" class="main-menu-access">
				<i class="icon-proton-logo"></i>
				<i class="icon-reorder"></i>
			</a>
		</nav>
		<?php require("header.php");?>

		<div class="main-grid">
			<!-- <div style="width: 100%;height: 500px;overflow: scroll;" height="80%"> -->
			<div class="agile-grids">	
				<!-- tables -->
				
				<div class="table-heading">
					<h2></h2>
				</div>
				<div class="agile-tables">
					<div class="w3l-table-info">
						<h3>Our Team</h3>
						<a href="our_team.php"  class="btn btn-primary pull-right" name="addbtn" id="addbtn" >Add Our Team</a><br><br>
						<table id="table">
							<thead>
							</thead>
							<?php
							include("../config.php");
/*
								$sql = "SELECT * from feedback";
								$result = mysqli_query($conn, $sql);*/
								$res = $conn->query('select * from chrysalis_details');	
								if($res->num_rows){

									?>
									<tr>
										
										<th>Sr.No</th>
										<th> Name</th>
										<th>Position</th>
										<th>Image</th>
										<th>Action</th>
										<th></th><th></th>
										<th></th><th></th>
									</tr>
								</thead>
								<tbody>
									<?php


								/*while($row = mysqli_fetch_assoc($result))
								{*/
									while ($row = $res->fetch_assoc())
									{

										?>
										<tr id = "tr-<?php echo $row['cry_id'];?>">

											<td class="center"><?php echo $row['cry_id'];?></td>
											<td class="center"><?php echo $row['cry_name'];?></td>
											<td class="center"><?php echo $row['cry_position'];?></td>

											<td class="center"><?php echo '<img src="'.$row['cry_profile'].'" width="100" class="img-thumbnail"/>'; ?>
											</td>
											<!-- <td style="width:3%;" ><a href="update_testimonial.php" class="btn btn-info" >Edit</button></td> -->
											<td style="width:3%;" ><form method="post" action="edit_ourteam.php" ><input type="text" name="cry_id" id="cry_id" value="<?php echo $row['cry_id']  ?>" style="display: none;" ><button type="submit" class="btn btn-info">Edit</button></form></td>

											<td style="width:3%;" ><button class="btn btn-primary" onclick="delete_ourteam(<?php echo $row['cry_id'];?>);">Delete</button></td>

											<td></td>
											<td></td>
											<td></td>
												
										<td>

										</td>
									</tr>

								</tbody>

								<?php } }?>
								<tr>
									
								</tr>
							</table>
						</div>
						<!-- //tables -->
					</div>
				</div>

				<div id="dModal" class="modal fade" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">

							<div class="modal-body text-center">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<p>Are you sure ?</p>	        
								<button type="button" class="btn btn-danger" id = "yes">Yes</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
								<div id = "dAlert" class="alert text-center hidden"></div>
							</div>
						</div>

					</div>
				</div>
				<!-- footer -->
				<?php require("footer.php") ?>
				<!-- //footer -->
			</section>
<!-- 	<script src="js/bootstrap.js"></script>
-->	
<script src="../sungare_admin/js/bootstrap.js"></script>
<!-- <script src="sungare_admin/js/datatable.js"></script>
<script>
	$(document).ready(function(){
    $('#table').DataTable();
});
</script> -->

<script>
	function delete_ourteam(id) {
		$('#dModal').modal('toggle');
		$('#yes').attr('onclick','del('+id+')');

	}

	function del(id){
		$.post('delete_ourteam.php', {cry_id : id}, function(data){
    	//if(data == 'OK'){
    		if(data.indexOf("1") >= 0 ){
    			$('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
    			window.setTimeout(function(){
    				$('#dModal').modal('toggle');
    				$('#tr-'+id).remove();
    			},1000);
    		}else{
    			$('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');    		
    		}

    		window.setTimeout(function(){
    			$('#dAlert').addClass('hidden').html('');
    		},1000);
    	});
	}
</script>
</body>
</html>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script type="text/javascript">
	myarray = [];
	function selectid(val)
	{
		

		if($('#'+val).prop('checked') == true)
		{
			myarray.push(val);
			console.log(myarray);

		}
		else
		{
			myarray.splice($.inArray(val,myarray),1);
			console.log(myarray);
		}
	}
	/*<!--------------------------select all---------------------------  -->*/
/*
	$('#select_all').on('click', function(e) {
		if($(this).is(':checked',true)) {
			$(".emp_checkbox").prop('checked', true);
		}
		else {
			$(".emp_checkbox").prop('checked',false);
		}

	});
	*/
	/*----------------------------delete record---------------------------------*/
	/*$('#delete_records').on('click',function()
	{
		if($('#select_all').prop('checked') == true)
		{
			$.ajax({
				url : 'deleteall.php',
				type : 'post',
				data : {'all' : 'all'},
				success : function(res)
				{
					console.log(res);
					if(res.indexOf('OK')>=0)
					{
					//getData();
					$('#select_all').prop('checked',false);
				}
			}
		});
		}
		else
		{

			$.ajax({
				url : 'deleteselected.php',
				type : 'post',
				data : {'myarray' : myarray},
				success : function(res)
				{
					console.log(res);
					if(res.indexOf('OK')>=0)
					{
					//getData();
				}
			}
		});
		}
		
	});*/
</script>
