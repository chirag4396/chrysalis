
<!DOCTYPE html>
<head>
	<title>Chrysalis</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Colored Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- bootstrap-css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- //bootstrap-css -->
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<!-- font CSS -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!-- font-awesome icons -->
	<link rel="stylesheet" href="css/font.css" type="text/css"/>
	<link href="css/font-awesome.css" rel="stylesheet"> 
		<!-- <script src="js/jquery.dataTables.min.js"></script>

		<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css"> -->
		<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">   
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<link rel="stylesheet" 

		href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css"></style>
		<script type="text/javascript" 

		src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" 

		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="../sungare_admin/js/jquery-1.11.1.min.js"></script>
		<script src="js/modernizr.js"></script>
		<script src="js/jquery.cookie.js"></script>
		<script src="js/screenfull.js"></script>

		<script>

			$(function () {
				$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

				if (!screenfull.enabled) {
					return false;
				}

				$('#toggle').click(function () {
					screenfull.toggle($('#container')[0]);
				});	
			});

		</script>


		<!-- tables -->

		<link rel="stylesheet" type="text/css" href="css/table-style.css" />
		<link rel="stylesheet" type="text/css" href="css/basictable.css" />
		<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#table').basictable();

				$('#table-breakpoint').basictable({
					breakpoint: 768
				});

				$('#table-swap-axis').basictable({
					swapAxis: true
				});

				$('#table-force-off').basictable({
					forceResponsive: false
				});

				$('#table-no-resize').basictable({
					noResize: true
				});

				$('#table-two-axis').basictable();

				$('#table-max-height').basictable({
					tableWrapper: true
				});
			});

		</script>

		<!-- //tables -->
	</head>
	<body class="dashboard-page">
		<?php require("nav_menu.php"); ?>

		<section class="wrapper scrollable">
			<nav class="user-menu">
				<a href="javascript:;" class="main-menu-access">
					<i class="icon-proton-logo"></i>
					<i class="icon-reorder"></i>
				</a>
			</nav>
			<?php require("header.php");?>

			<div class="main-grid">
				<div class="agile-grids">	
					<!-- tables -->

					<div class="table-heading">
						<h2></h2>
					</div>
					<div class="agile-tables">
						<div class="w3l-table-info">
							<h3>Quote By MG</h3>
							<a href="quotes_by_MG.php"  class="btn btn-primary pull-right" name="addbtn" id="addbtn" >Add Quote By MG</a>
							<!-- <table id="table"> -->
							<table id="example" class="display" cellspacing="0" width="100%">
								<thead>
								</thead>
								<?php
								include("../config.php");

								$res = $conn->query('select * from quote_mg inner join categories on categories.test_cat_id = quote_mg.test_cat_id');
								if($res->num_rows){

									?>
									<tr>

										<th>Sr.No</th>
										<th> Category </th>
										<th> Image</th>
										<th>Action</th>
										<th></th><th></th>
										<th></th><th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php


									while ($row = $res->fetch_assoc())
									{
										
										?>
										<tr id = "tr-<?php echo $row['quote_id'];?>">

											<td class="center"><?php echo $row['quote_id'];?></td>
											
											<td class="center"><?php echo $row['test_cat_name'];?></td>


											<td class="center"><?php echo '<img src="'.$row['quote_img'].'" width="100" class="img-thumbnail"/>'; ?>
											</td>

											<td style="width:3%;" ><form method="post" action="update_quotemg_details.php" ><input type="text" name="quote_id" id="quote_id" value="<?php echo $row['quote_id']  ?>" style="display: none;" ><button type="submit" class="btn btn-primary">Edit</button></form></td>

											<td style="width:3%;" ><button class="btn btn-primary" onclick="delete_quotemg(<?php echo $row['quote_id'];?>);">Delete</button></td>

											<td></td>
											<td></td>
											<td></td>

											<td>

											</td>
										</tr>

									</tbody>

									<?php } }?>
									<tr>

									</tr>
								</table>
							</div>
							<!-- //tables -->
						</div>
					</div>
					<div id="dModal" class="modal fade" role="dialog">
						<div class="modal-dialog">

							<!-- Modal content-->
							<div class="modal-content">

								<div class="modal-body text-center">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<p>Are you sure ?</p>	        
									<button type="button" class="btn btn-danger" id = "yes">Yes</button>
									<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
									<div id = "dAlert" class="alert text-center hidden"></div>
								</div>
							</div>

						</div>
					</div>
					<!-- footer -->
					<?php require("footer.php") ?>
					<!-- //footer -->
				</section>
<!-- 	<script src="js/bootstrap.js"></script>
-->	
<script src="../sungare_admin/js/bootstrap.js"></script>

<script>
	function delete_quotemg(id) {
		$('#dModal').modal('toggle');
		$('#yes').attr('onclick','del('+id+')');

	}

	function del(id){
		$.post('delete_quotemg.php', {quote_id : id}, function(data){
    	//if(data == 'OK'){
    		if(data.indexOf("1") >= 0 ){
    			$('#dAlert').removeClass('hidden').addClass('alert-success').html('Deleted Successfully!!');
    			window.setTimeout(function(){
    				$('#dModal').modal('toggle');
    				$('#tr-'+id).remove();
    			},1000);
    		}else{
    			$('#dAlert').removeClass('hidden').addClass('alert-danger').html('Something wen\'t wrong, Please try again!!');    		
    		}

    		window.setTimeout(function(){
    			$('#dAlert').addClass('hidden').html('');
    		},1000);
    	});
	}

</script>
</body>
</html>
<!-- <script type="text/javascript" src="js/bootstrap.js"></script>
-->
<!--  <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
-->

<script type="text/javascript">
	myarray = [];
	function selectid(val)
	{
		

		if($('#'+val).prop('checked') == true)
		{
			myarray.push(val);
			console.log(myarray);

		}
		else
		{
			myarray.splice($.inArray(val,myarray),1);
			console.log(myarray);
		}
	}

</script>
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js
"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js

"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.1/js/responsive.bootstrap.min.js

"></script>
" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css
"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.bootstrap.min.css

"/>


<script type="text/javascript">
	$(document).ready(function(){ 
    $('#example').DataTable();
      }); 

  </script> -->

 <!-- <script type="text/javascript">
$(document).ready(function(){
    $('#example').dataTable();
});
</script>
 -->