<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>

</div><br><br>


<div class="container-full padding-top-bottom">
    <div class="container initiatives">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50">
                <h4 class="section-title">Initiatives</h4>
                <div class="bottom-line"></div>
            </div>
            <br class="clearfix">
            <div class="col-lg-12 margin-bottom-50">
                <ul>
                    <div class="circle top"></div>
                    <div class="circle bottom"></div>
                        <?php
                            include("config.php");
/*
                                $sql = "SELECT * from feedback";
                                $result = mysqli_query($conn, $sql);*/
                                $res = $conn->query('select * from tcf_details');   
                                if($res->num_rows){
                                    while ($row = $res->fetch_assoc())
                                    {

                                    ?>
                    <li>
                        <div class="data">
                            <div class="date"><span>From</span><?php echo $row['tcf_year'];?></div>
                            <div class="initiative-img">
                                <img src="sungare_admin/<?php echo $row['tcf_profile'];?>">
                            </div>
                            <div class="initiative-details">
                                <h5><?php echo $row['tcf_name'];?></h5>
                                <p><?php echo $row['tcf_description'];?></p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <?php } }?>
                    <!-- <li>
                        <div class="data">
                            <div class="date">2010</div>
                            <div class="initiative-img">
                                <img src="images/initiative-02.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Association with Manavya</h5>
                                <p>Manavya is a home for nursing, caring and sheltering orphaned and destitute children and women afflicted by HIV/AIDS. The foundation raised funds for the organisation by organizing power talks titled “I – The Miracle”.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2010</div>
                            <div class="initiative-img">
                                <img src="images/initiative-01.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Roti, Kapda, Muskan<br>
                                (An initiative to collect rice and clothes for the tribals of Velhe)
                                </h5>
                                <p>Manavya is a home for nursing, caring and sheltering orphaned and destitute children and women afflicted by HIV/AIDS. The foundation raised funds for the organisation by organizing power talks titled “I – The Miracle”.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2012</div>
                            <div class="initiative-img">
                                <img src="images/initiative-01.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Mission Jal Samruddhi <br>
                                (Initiative to desilt a 100-acre lake to provide potable water in Beed)
                                </h5>
                                <p>TCF initiated “Project Jal Samruddhi” to desilt the 100 acre Nizam Era Twarita Devi Pond at Talawade in Beed District by raising funds to the tune of well over Rs. 30 Lakhs for this project. This project resulted in 1500 acres of land becoming fertile and 60 crore liters of water generation for the area.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2013</div>
                            <div class="initiative-img">
                                <img src="images/initiative-01.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>One Day for My City</h5>
                                <p>It is an initiative to encourage the citizens of the city to give one day periodically for the betterment of the city.some of the activities undertaken under this initiative are Cleaning of PMPML buses, Creating awareness among citizens to participate in the electoral process by exercising their right to vote in the Recent Lok Sabha Elections Participating in the Swachch Bharat Abhiyaan.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>