
        <!-- footer start -->
        <footer>
            <!-- footer-top-area start -->
            <div class="footer-top-area">
                <div class="container">
                    <div class="row">
                        <!-- footer-widget start -->
                        <div class="col-lg-3 col-md-3 col-sm-4">
                            <div class="footer-widget mft">                                

                                <h3>Our Vision</h3>
                                <p style="">By 2020 creating one million success stories and empowering 10,000 Entrepreneurs</p>
                                    <div class="widget-icon">
                                        <a href="https://www.instagram.com/mg_chrysalis" target="_blank" class="insta"><i class="fa fa-instagram"></i></a>
                                        <a href="https://www.youtube.com/channel/UCeXq7ztkYXtkLB-27LrdCTQ" target="_blank" class="yout"><i class="fa fa-youtube"></i></a>
                                        <a href="https://twitter.com/Mgchrysalis" target="_blank" class="twit"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.facebook.com/Chrysalis-Empowers-1712411335714165/?fref=ts" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
                                    </div>
                            </div>
                        </div>
                        <!-- footer-widget end -->
                        <!-- footer-widget start -->
                        <div class="col-lg-3 col-md-3 hidden-sm">
                            <div class="footer-widget">
                                <h3>Our services</h3>
                                <ul class="footer-menu" style="margin-bottom: 0px;">
                                    <li><a href="?php echo $chrysalisPath; ?>aboutus.php">about us</a></li>
                                    <li><a href="<?php echo $chrysalisPath; ?>cef.php">CEF</a></li>
                                    <li><a href="<?php echo $chrysalisPath; ?>businessOfferings.php">program</a></li>
                                    <li><a target="_blank" href="http://www.manishgupta.co.in/mg-as-keynote-speaker/">Invite MG</a></li>
                                    <li><a style="border-bottom: none;" href="<?php echo $eventPath; ?>">Blog by chrysalis</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- footer-widget end -->
                        <!-- footer-widget start -->
                        <div class="col-lg-3 col-md-3 col-sm-4">
                            <div class="footer-widget">
                                <h3>Our services</h3>
                                <ul class="footer-menu"  style="margin-bottom: 0px;">
                                    <li><a href="<?php echo $chrysalisPath; ?>clients.php">Our clients</a></li>
                                    <li><a href="<?php echo $chrysalisPath; ?>clients.php">clients</a></li>
                                    <li><a href="<?php echo $chrysalisPath; ?>testimonials.php">Testimonials</a></li>
                                    <li><a href="<?php echo $eventPath; ?>career/">careers</a></li>
                                    <li><a style="border-bottom: none;" href="<?php echo $eventPath; ?>events-list/">events</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- footer-widget end -->
                        <!-- footer-widget start -->
                        <div class="col-lg-3 col-md-3 col-sm-4">
                            <div class="footer-widget">
                                <h3>CONTACT US</h3>
                                <ul class="footer-contact" style="margin-bottom: 0px;">
                                    <li>
                                        <i class="fa fa-map-marker"> </i>
                                        Addresss: Chrysalis, 1, Beena, Chrysalis Corner, Senapati Bapat Road, Pune, 411016, Maharashtra, India. 
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope"> </i> 
                                        Email: emailus@chrysalis.net.in
                                    </li>
                                    <li>
                                        <i class="fa fa-phone"> </i>
                                        Phone: 020-25676987 <br>
                                        Mobile: +91-823-700-8400 
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- footer-widget end -->
                    </div>
                </div>
            </div>
            <!-- footer-top-area end -->
            <!-- footer-bottom-area start -->
            <div class="footer-bottom-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="copyright">
                                <p>Copyright © <a href="http://sungare.com/">Sungare</a>. All Rights Reserved</p>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- footer-bottom-area end -->
        </footer>
        <!-- footer end -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-77719520-1', 'auto');
          ga('send', 'pageview');

        </script>
        <!-- inject:js -->
        <script src="js/vender/jquery.js"></script>
        

        <script src="js/vender/bootstrap.min.js"></script>
        <script src="js/vender/jquery.easing.min.js"></script>
        <script src="js/vender/jquery.isotope.min.js"></script>
        <script src="js/vender/owl.carousel.min.js"></script>
        <script src="js/vender/jquery.prettyPhoto.js"></script>
        <script src="js/vender/jquery.plugin.validation.js"></script>
        <script src="js/custom/custom.js"></script>
        <!-- endinject -->
        <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'd0f8fc2a-5cd5-42d7-9a04-8aa1880b9919', f: true }); done = true; } }; })();</script>


        <!-- animation effects-js files-->
        <script src="js/custom/aos.js"></script><!-- //animation effects-js-->
        <script src="js/custom/aos1.js"></script><!-- //animation effects-js-->
        <!-- animation effects-js files-->
        <script type="text/javascript" src="js/custom/easing.js"></script>

        <script type="text/javascript">
            
            var easeOutBounce = function (x, t, b, c, d) {
                    if ((t/=d) < (1/2.75)) {
                        return c*(7.5625*t*t) + b;
                    } else if (t < (2/2.75)) {
                        return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
                    } else if (t < (2.5/2.75)) {
                        return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
                    } else {
                        return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
                    }
            }

            function Animate(elem, propName, duration, start, end)  {
                var start_time = new Date().getTime();
                var interval = setInterval(function() {
                  var current_time = new Date().getTime(),
                    remaining = Math.max(0, start_time + duration - current_time),
                    temp = remaining / duration || 0,
                    percent = 1 - temp;

                  if (start_time + duration < current_time) clearInterval(interval);

                  var pos = easeOutBounce(null, duration * percent, 0, 1, duration),
                    current = (end - start) * pos + start;

                  elem.style[propName] = current + 'px';
                }, 1);
              }

            var elem = document.getElementById('contactform');
            var opened = false; 

            document.getElementById('contact-button').onclick = function() {
              if (opened) {
                Animate(elem, 'left', 800, 0, -405);    
                opened = false;
              } else {
                Animate(elem, 'left', 800, -405, 0);
                opened = true;  
              }  
            }
        </script>



        <script type="text/javascript">
            function sticky_relocate() {
            var window_top = $(window).scrollTop();
            var div_top = $('#sticky-anchor').offset().top;
            if (window_top > div_top) {
                $('#myTab').addClass('stick');
                $('#sticky-anchor').height($('#myTab').outerHeight());
            } else {
                $('#myTab').removeClass('stick');
                $('#sticky-anchor').height(0);
            }
        }

        $(function() {
            $(window).scroll(sticky_relocate);
            sticky_relocate();
        });

        var dir = 1;
        var MIN_TOP = 200;
        var MAX_TOP = 350;

        function autoscroll() {
            var window_top = $(window).scrollTop() + dir;
            if (window_top >= MAX_TOP) {
                window_top = MAX_TOP;
                dir = -1;
            } else if (window_top <= MIN_TOP) {
                window_top = MIN_TOP;
                dir = 1;
            }
            $(window).scrollTop(window_top);
            window.setTimeout(autoscroll, 100);
        }

        </script>

    </body>
</html>