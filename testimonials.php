<?php include 'config.php';?>
<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    <section class="inner-banner-background">
        <img src="images/1920x534/testimonials.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/testimonials.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/testimonials.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
         <p>
            <!-- <i class="fa fa-diamond"></i> -->
            Our Client Talk<br>
        </p>
        <div class="bottom-line"></div>
    </div>
</div>

<div class="container-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 padding-top-bottom testimonials-container">
             
                <div class="testimonial-filter">
                    <ul>
                        <li class="margin-left-0" id="filter-all">All</li>

                        <?php 

                      /*  $res = $conn->query('select * from categories');
*/
                        $res = $conn->query('select * from categories where type_id=3');

                        while ($rowC = $res->fetch_assoc()) {
                        
                        echo '<li id="filter-'.preg_replace('/\s/','-',$rowC['test_cat_name']).'">'.$rowC['test_cat_name'].'</li>';
                            
                        }
                        ?>
                         <div class="clearfix"></div>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="testi-container">
                    <?php 
                        $resT = $conn->query('select * from testimonial inner join categories on categories.test_cat_id = testimonial.test_category');

                        while ($rowT = $resT->fetch_assoc()) {
                            
                            ?>
                        <div class="col-lg-4 col-md-4 col-sm-12 testimonial filter-all filter-<?php echo preg_replace('/\s/','-',$rowT['test_cat_name']); ?>">
                            <div class="testimonial-top">
                                <?php if (!is_null($rowT['test_link'])): ?>
                                    
                                <iframe width="100%" height="auto" src="<?php echo $rowT['test_link'] ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                <?php endif ?>
                                <?php if (!is_null($rowT['test_msg'])): ?>
                                    <p><?php echo $rowT['test_msg'] ?></p>
                                <?php endif ?>
                                <h6><?php echo $rowT['test_name']; ?><br>
                                    <span><?php echo $rowT['test_company_name']; ?></span>
                                </h6>
                            </div>
                            <div class="testimonial-user">
                                <img src="<?php echo 'sungare_admin/'.$rowT['test_image']; ?>" >
                            </div>
                        </div>
                    <?php
                        }
                    ?>
                  
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>