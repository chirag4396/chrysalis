							  <?php echo 'is page==>'.is_page();?>
<aside class="right rightAside">
                
                <div class="one-third column alpha omega blogSidebarWidth">
                    
                         <div class="rightAsideBox">
                    
                         
                          
                          <div id="shareButtons">
                          
                          <h3>Share</h3>
                        
                                
                                <ul>
                            
                              <li>
                              
                              
                               <a target="_blank" href="https://www.facebook.com/sharer.php?u=<?php 
							    //if ( is_page( 'category-blog.php' ) ) {
									
                                   echo urlencode(get_permalink($post->ID));
								/*}else if ( is_page( 'gallery-single.php' ) ) {
                  echo urlencode(get_permalink($post->ID));
                }
								else {
									echo urlencode('http://manishgupta.co.in');
								}*/
							  
							   ?>">Facebook</a>
                              
                              
                              </li>
                              <li>
                              
                              <a target="_blank" href="http://twitter.com/share?text=<?php
							  
							   echo urlencode('Manish Gupta : This is the official website of Dr. Manish Gupta (MG), Chairman, Chrysalis Group.
'); ?>&url=<?php echo urlencode('http://manishgupta.co.in'); ?>; 
							   
							   
							   ?>">Twitter</a>
                               
                               
                               </li>
                              <li>
                              
                              <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=http://www.manishgupta.co.in/&title=Manish%20Gupta&summary=This%20is%20the%20official%20website%20of%20Dr.%20Manish%20Gupta%20(MG),%20Chairman,%20Chrysalis%20Group.&source=">Linkedin</a>
                               
                               </li>                              
                              <li><a href="mailto:?subject=Manish Gupta &body=This is the official website of Dr. Manish Gupta (MG), Chairman, Chrysalis Group. http://manishgupta.co.in" rel="nofollow" title="Email"><span class="instaemail-text">Email</span></a></li>
                              
                             
                            </ul>
                            
                            
                           
  						</div>                    
                    <div class="clear"></div>
                    	
                    </div>
                    
                    	 <div class="rightAsideBox">
                    
                        
                          <div id="subscribe">
                          	<h3 class="widget-title">Subscribe for emailer</h3>
                          </div>
                          <div class="subscribeBox">
                          
                               <!--   <input type="text" placeholder="My Email ID is">
                                  <a href="#;" class="submtBtn">Submit</a>-->
  
					 		<ul>
								<?php
									dynamic_sidebar('primary-widget-area');
								?>
					    	</ul>
                                <div class="clear"></div>
                          
                          </div>                      
                    
                    	
                    </div>
                    	
                    <div class="rightAsideBox">
                    
                            
                              
                              <div id="mostRead">
   								 <h3>Most Read</h3>
                              </div>
                              
                              <?php
setPostViews(get_the_ID()); ?>   
                              
                             
                              
                                  
                                 <?php

query_posts('meta_key=post_views_count&orderby=meta_value_num&order=DESC');

if (have_posts()):
    while (have_posts()):
        the_post(); ?>
                                
								 <div class="mostReadBox">
                                 
                                        <div class="mostReadImg">                                 
                                     
                                             <a href="<?php
        the_permalink() ?>" rel="bookmark" title="<?php
        the_title(); ?>" >
                                    
                                                    <?php
        the_post_thumbnail('small'); ?>
                                        
                                             </a>
                                             
                                        </div>                            
                            
                                        <div class="mostReadCopy">
                                        
                                                <a href="<?php
        the_permalink() ?>" title="<?php
        the_title(); ?>">
                            
                                                 <p><?php
        the_title(); ?></p>
                                
                                                </a>
                                        
                                                <p>By: <span class="orangeText"><?php
        the_author_posts_link(); ?></span></p>
                                                                                   
                                        </div>
                                    
                                  		<div class="clear"></div>
                                  
                               </div>
								<?php
    endwhile;
endif;
wp_reset_query();
?>   
                                    
                                  
                              
                             
                              
                            
                               <a href="<?php
echo esc_url(home_url('/category/blog')); ?>" class="moreArticleLink">More Articles</a>	  
                              
                    
                    </div>
                            
                            
                            
                    
                      	<div class="clear"></div>
                    	
                    </div>
                    
                        
                	
                
                </aside>