<html>
<head>
    <title>Thank you</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
        tr td {
            margin: 0px;
            border: 0px;
        }
          body {
           padding-top: 0 !important;
           padding-bottom: 0 !important;
           padding-top: 0 !important;
           padding-bottom: 0 !important;
           margin:0 !important;
           width: 100% !important;
           -webkit-text-size-adjust: 100% !important;
           -ms-text-size-adjust: 100% !important;
           -webkit-font-smoothing: antialiased !important;
         }
          img {
           border: 0 !important;
           display: block !important;
           outline: none !important;
         }
    </style>
</head>

<body bgcolor="#d0d0d0" paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; font-family:Verdana, Geneva, sans-serif;" offset="0" toppadding="0" leftpadding="0">
<div style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; font-family:Verdana, Geneva, sans-serif;background: #d0d0d0;" offset="0" toppadding="0" leftpadding="0">
    <table width="600" border="0" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" align="center" style="font-family:Verdana, Geneva, sans-serif;text-align:center">
        <tbody>
            <tr>
                <td width="600" height="50" bgcolor="#d0d0d0">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="20" valign="top" bgcolor="#d0d0d0" style="text-align:left;"></td>
                            <td width="330" height="47" valign="top" bgcolor="#d0d0d0" style="text-align:left;">
                                <img src="http://www.chrysalis.net.in/includes/images/toUser/ticketLogo.jpg">
                            </td>
                            <td width="230" height="47" bgcolor="#d0d0d0">
                                <p style="font-family:Verdana, Geneva, sans-serif;color: #747474;font-size: 11px;text-align: right;line-height: 16px;">Please carry a digital format <br>
                                of this ticket to get entry to the event.</p>
                            </td>
                            <td width="20" valign="top" bgcolor="#d0d0d0" style="text-align:left;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="600" height="20" bgcolor="#d0d0d0">&nbsp;</td>
            </tr>
            <tr>
                <td width="600" bgcolor="#ffffff">
                    <p style="font-family:Verdana, Geneva, sans-serif;color: #747474;font-size: 14px;text-align: left;line-height: 22px; padding: 50px 30px 30px 30px">Dear <?php echo $fName; ?><br>
You have Successfully Registered to the Event!</p>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="20" valign="top" bgcolor="#FFFFFF" style="text-align:left;"></td>
                            <td width="358" valign="top" bgcolor="#f68b1f" style="text-align:left;">
                                <h1 style="font-family:Verdana, Geneva, sans-serif;color: #ffffff;font-size: 18px;text-align: left;font-weight:700;line-height: 22px;padding:20px 30px 10px 30px;text-transform:uppercase;"><?php echo $event_name;?>  </h1>
                                <p style="font-family:Verdana, Geneva, sans-serif;color: #ffffff;font-size: 12px;text-align: left;line-height: 20px;padding: 0px 30px 20px 30px;">
                                <?php echo $event_venue; ?>
                                
                            </td>
                            <td width="200" bgcolor="#f68b1f" style="border-left:2px solid #ffffff;">
                                <table width="200" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="100" valign="top" bgcolor="#f68b1f" style="text-align:left;">
                                            <p style="font-family:Verdana, Geneva, sans-serif;color: #ffffff;font-size: 12px;font-weight: 700;text-align: left;line-height: 20px;padding: 20px 20px 20px 20px;">
                                            Date - <?php echo $event_date; ?><br>
                                            Time - <?php echo $event_time; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="200" valign="top" bgcolor="#595756" style="text-align:left;">
                                            <p style="font-family:Verdana, Geneva, sans-serif;color: #ffffff;font-size: 12px;text-align: left;line-height: 20px;padding: 20px 20px 20px 20px;">
                                            E - Ticket</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="20" valign="top" bgcolor="#FFFFFF" style="text-align:left;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="20" valign="top" bgcolor="#FFFFFF" style="text-align:left;"></td>
                            <td width="560" valign="top" bgcolor="#f2efee" style="text-align:left;">
                                <h3 style="font-family:Verdana, Sans-serif;font-size:14px; color:#797979;line-height:22px;padding:30px 30px 0px 30px;margin:0px 0px 0px 0px;">You can also contact us on</h3>
                                <p style="font-size:13px; color:#797979;line-height:22px;padding:10px 30px 30px 30px;margin:0px 0px 0px 0px;">
                                Phone - +91 8237008400 / 02025676987 or <br>
                                Email - <a style="font-size:13px; color:#797979;line-height:22px;text-decoration: none;" href="emailTo:emailus@chrysalis.net.in">emailus@chrysalis.net.in</a></p>
                            </td>
                            <td width="20" valign="top" bgcolor="#FFFFFF" style="text-align:left;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="600" height="50">&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="20" valign="top" bgcolor="#d0d0d0" style="text-align:left;"></td>
                            <td width="350" height="47" valign="top" bgcolor="#d0d0d0" style="text-align:left;">
                                <a style="font-family:Verdana, Sans-serif;font-size: 12px;color: #ffffff;text-indent: 35px;line-height: 55px;text-decoration: none;" href="http://www.chrysalis.net.in/" target="_blank">www.chrysalis.net.in  |  </a>
                                <a style="font-family:Verdana, Sans-serif;font-size: 12px;color: #ffffff;text-indent: 35px;line-height: 55px;text-decoration: none;" href="http://www.manishgupta.co.in/" target="_blank">www.manishgupta.co.in</a>
                            </td>
                            <td width="210" height="47" bgcolor="#d0d0d0">
                                &nbsp;
                            </td>
                            <td width="20" valign="top" bgcolor="#d0d0d0" style="text-align:left;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="600" height="50" bgcolor="#d0d0d0">&nbsp;</td>
            </tr>
        </tbody>
    </table>
    </div>
</body>

</html>