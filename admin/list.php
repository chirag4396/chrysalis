<?php 
  session_start();
  if(isset($_SESSION['username']) && isset($_SESSION['password']))
  {
      // include_once('../MysqliDb.php');
      include_once('../includes/DbConnect.php');
      error_reporting(E_ALL);
      ini_set('display_errors', '0');

      $db = new MysqliDb ();
      $db->orderBy("date_added","Desc");
      $results = $db->get('tbl_newsletters');
      // print_r($results);
     ?>
    <!doctype html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html>
    <!--<![endif]-->

    <head>
        <!-- Meta Tag -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

        <style>
            body {
                font-family: 'Open Sans', sans-serif;
            }
    	     .table-responsive table {
    		        margin-top: 20px;
    	     }
        	.table-responsive thead tr {
        		background-color: #333333;
        		color: #ffffff;
        	}
                .table-responsive tbody tr:nth-child(even) {
        		background-color: #f1f1f1;
        	}
        </style>
      

    </head>

    <body>
    <div class="container">
      <h2>User Login Details:</h2>
     <span> 
          <p style="float:left">The table displays the users registered for newsletters.</p> 
          <button style="margin-left: 630px" class="btn" type="submit" id="logout">Logout</button>
        <a href="#" id="export" onclick=""> <img src="images/xls.png" width="24px" > Export</a>
     </span>
      <div class="table-responsive">          
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Contact</th>
          </tr>
        </thead>
        <tbody>
            
        <?php
          $i=1;
      foreach ($results as $key => $value) {
      ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $value['name']; ?></td>
            <td><?php echo $value['email']; ?></td>
            <td><?php echo $value['phone']; ?></td>
          </tr>
        <?php   $i++; }
        
        ?>

          <!-- <tr>
            <td>1</td>
            <td>Vikas Satpute</td>
            <td>Vikas.s.satpute@gmail.com</td>
            <td>9876543210</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Vikas Satpute</td>
            <td>Vikas.s.satpute@gmail.com</td>
            <td>9876543210</td>
          </tr> -->
        </tbody>
      </table>
      </div>
    </div>


        <!-- Bootstrap core JavaScript
        	================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')
        </script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="tableExport.js"></script>
        <script type="text/javascript" src="jquery.base64.js"></script>   
          <script type="text/javascript">
          $('#export').on('click',function(){
               /* $.get('exportExcel.php',{},function(data){
                  console.log('download excel');
                })*/
                // alert("afaaf");
                window.location = "http://chrysalis.net.in/admin/exportExcel.php";
            });
            $('#logout').click(function(){
              console.log('click');
                $.post('logout.php',{logout:'logout'},function(data){
                  if(data=='1'){
                   window.location = "http://www.chrysalis.net.in/";
                  }
                })
            });
        </script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug 
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
    </body>

    </html>
    <?php } else{
      echo "<h2><center> Please login to get access!</center></h2>";
      header('Location:http://www.chrysalis.net.in');
      } ?>