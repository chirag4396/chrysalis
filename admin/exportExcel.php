<?php 
      ob_start(); 
      include_once('../includes/DbConnect.php');                    //local
      // include_once('../includes/DbConnect.php');       //server
      error_reporting(E_ALL);
      ini_set('display_errors', '0');

      $db = new MysqliDb ();
      $db->orderBy("date_added","Desc");
      $results = $db->get('tbl_newsletters');
      // print_r($results);
      
      ini_set('display_errors', FALSE);
      ini_set('display_startup_errors', FALSE);
      date_default_timezone_set('Europe/London');
      if (PHP_SAPI == 'cli')
        die('This example should only be run from a Web Browser');
      /** Include PHPExcel */
      require_once dirname(__FILE__) . '/PHPExcel/Classes/PHPExcel.php';
      // Create new PHPExcel object
      $objPHPExcel = new PHPExcel();
      // Set document properties
      $objPHPExcel->getProperties()->setCreator("Chrysalis")
                     ->setLastModifiedBy("Chrysalis")
                     ->setTitle("Office 2007 XLSX Test Document")
                     ->setSubject("Office 2007 XLSX Test Document")
                     ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                     ->setKeywords("office 2007 openxml php")
                     ->setCategory("Newsletter List file");
      
     ?>
   
        <?php
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A1", "Name")
            ->setCellValue("B1", "Email")
            ->setCellValue("C1", "Phone");
          $i=2;
      foreach ($results as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A$i", $value['name'])
            ->setCellValue("B$i", $value['email'])
            ->setCellValue("C$i", $value['phone']);
        $i++; 
      }
        ?>
    <?php 
     // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        ob_end_clean();
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Chrysalis.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;