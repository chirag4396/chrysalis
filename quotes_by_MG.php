<?php include 'config.php';?>
<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 padding-top-bottom">
                <!-- <h4 class="section-title">Business Offerings</h4>
                <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                    Programs For Entrepreneurs And Their Teams
                </p> -->


                <div class="contact_tab myspace">
                    <ul id="myTab" class="container text-center nav nav-tabs" role="tablist">
                        <?php 
                        $res = $conn->query('select * from categories where type_id = 1');
                        if($res->num_rows){
                            $k = 0;
                            while ($row = $res->fetch_assoc()) {

                                echo '<li '.(($k == 0) ? 'class="active"' : '').' id = "tgob-li"><a href="#tab'.$row['test_cat_id'].'" role="tab" data-toggle="tab">'.$row['test_cat_name'].'</a></li>';
                                $k++;
                            }
                        }
                        ?>                        
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <?php 
                        $res2 = $conn->query('select * from categories where type_id = 1');

                        if($res2->num_rows){
                            $i = 0;
                            while ($row2 = $res2->fetch_assoc()) {

                                ?>
                                <div class="tab-pane fade <?php echo ($i==0 ? 'in active' : ''); ?>" id="tab<?php echo $row2['test_cat_id']; ?>">
                                    <div class="program-info">                                        
                                        <div class="gallery" id="projects">
                                            <div class="container">

                                                <div class="filtr-container">
                                                    <?php
                                                    $resinn = $conn->query('SELECT * FROM quote_mg where test_cat_id =1'); 

                                                    if($resinn->num_rows){

                                                        while ($rowi = $resinn->fetch_assoc()) {

                                                            ?>

                                                            <div class=" filtr-item text-center" data-category="" data-sort="Luminous night">
                                                                <a href="<?php echo 'sungare_admin/'.$rowi['quote_img'];?>" class="b-link-stripe b-animate-go  thickbox">
                                                                    <figure>
                                                                        <img src="<?php echo 'sungare_admin/'.$rowi['quote_img'];?>" class="img-responsive" alt=" "  style="background-image: url(<?php echo 'sungare_admin/'.$rowi['quote_img'];?>);"/><figcaption>
                                                                            <h3>CHRYSALIS</h3>
                                                                        </figcaption>
                                                                    </figure>
                                                                </a>
                                                            </div>
                                                            <?php 
                                                        }
                                                    }
                                                    ?>

                                                    <div class="clearfix"> </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div><!-- End Tab Pane -->
                                <?php

                                $i++;
                            }
                        }
                        ?>  


                       <!--  <div class="tab-pane fade" id="pankh">
                            <div class="program-info">
                                <h4></h4> 
                            </div>

                            <div class="gallery" id="projects">
                                  <div class="filtr-container">  
                                      <div class=" filtr-item" data-category="2" data-sort="Luminous night">
                                          <a href="images/event1.JPG" class="b-link-stripe b-animate-go  thickbox">
                                            <figure>
                                                <img src="images/event1.JPG" class="img-responsive" alt=" " />  <figcaption>
                                                <h3>Belas Creation</h3>
                                                <p>
                                                  We are created to create
                                                </p>
                                              </figcaption>
                                            </figure>
                                          </a>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/7U_ZT9K0uco" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/BwCH04pZ9Rc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>

                                      <div class=" filtr-item" data-category="4" data-sort="Luminous night">
                                          <iframe width="100%" height="280px" src="https://www.youtube.com/embed/7U_ZT9K0uco" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                      </div>                                       
                                      <div class="clearfix"> </div>
                                  </div>
                              </div>     
                          </div> -->
                      </div>

                  </div><!-- /end my tab content -->
              </div><!-- /contact_tab -->


          </div>
      </div>
  </div>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">


            <div class="container">
                <div class="row">

                    <!-- Form -->
                    <div class="nb-form">
                        <p class="title text-center">Send Enquiry</p>   

                        <form method="POST" role="form" id="myform">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="" placeholder="Enter Full Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="" placeholder="Enter Email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" id="" placeholder="Enter Phone No.">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="inputMessage" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <center>
                                <input type="submit" name="name" id="submit" value="Submit" class="btn btn-default">
                            </center>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uprog-lnk">
                <p>To know about our upcoming program <a href="<?php echo $eventPath; ?>events-list/">click here</a></p>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

<script type="text/javascript" src="js/bootstrap.js"></script>

<script src="js/jquery.filterizr.js"></script>

<script type="text/javascript">
    $(function() {
        $('.filtr-container').filterizr();
    });
</script>
<!---->
<script src="js/jquery.chocolat.js"></script>
<!--light-box-files -->
<script>
    $(function() {
        $('.filtr-item a').Chocolat();
    });
</script>  

