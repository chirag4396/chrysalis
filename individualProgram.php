<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
</div>


<p>
    <button onclick="autoscroll()"></button>
</p>

<div id="sticky-anchor"></div>
<div id="sticky"></div>

<div class="padding-top2">
    <ul id="myTab" class="container text-center nav nav-tabs navbar-fixed-top2" role="tablist">
        <li class="active" id = "tgob-li"><a href="#tgob" role="tab" data-toggle="tab">Life Leadership Program</a></li>
        <li id = "pankh-li"><a href="#pankh" role="tab" data-toggle="tab">My life </a></li>
        <li id = "tl-li"><a href="#tl" role="tab" data-toggle="tab">Ehsaas</a></li>
    </ul>
</div>

<div class="container-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="contact_tab myspace">
                   
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="tgob">
                            <div class="container">
                                <div class="program-info">
                                    <h4>Life Leadership Program</h4>
                                    <p>It's in this one life we got to do what all we can. There is no second chance, no second innings, just this one life...
                                    </p>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul>
                                            <li><i class="fa fa-circle"></i> Building Vision and Purpose of Life</li>
                                            <li><i class="fa fa-circle"></i> Should we then not understand that there must be a better way to live?</li>
                                            <li><i class="fa fa-circle"></i> Should we then not pick up a few tools to live better?</li>
                                            <li><i class="fa fa-circle"></i> Do you discuss your CHALLENGES with yourself?</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul>
                                            <li><i class="fa fa-circle"></i> Should you manage TIME or LIFE?</li>
                                            <li><i class="fa fa-circle"></i> Are you doing justice to your RELATIONSHIPS?</li>
                                            <li><i class="fa fa-circle"></i> Should we then not fast forward our life by benefiting from the wisdom of others?</li>
                                            <li><i class="fa fa-circle"></i> Should we then not explore how to maximise from life?</li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <br/><br/>
                                    <p>There is so much more life can offer to each one of us and Life is never partial, it's the way it is. It's just that it gives maximum to those who embrace it, to those who understand it, to those who conduct themselves in accordance to the game of life. Chrysalis presents Life Leadership Program, which is an exclusive opportunity for those rare individuals committed to connecting, contributing and growing on a massive scale. This program will give you the tools and experiences to master the skills of lasting change and step into your own greatness as an individual and maximising on your potential.</p>
                                    <p>Life Leadership Program is 4 days residential program.</p>

                                    <h6>Learn</h6>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul>
                                            <li><i class="fa fa-circle"></i> Building Vision and Purpose of Life</li>
                                            <li><i class="fa fa-circle"></i> Goal Setting & Decision Making</li>
                                            <li><i class="fa fa-circle"></i> Multiplying Confidence</li>
                                            <li><i class="fa fa-circle"></i> Empowering Communication</li>
                                            <li><i class="fa fa-circle"></i> Intricacies of Human Dynamics</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul>
                                            <li><i class="fa fa-circle"></i> Power of Self Talk</li>
                                            <li><i class="fa fa-circle"></i> Managing Time</li>
                                            <li><i class="fa fa-circle"></i> Nurturing Relationships</li>
                                            <li><i class="fa fa-circle"></i> Infusing Enthusiasm</li>
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- End Container -->
                        </div><!-- End Tab Pane -->

                        <div class="tab-pane fade" id="pankh">
                            <div class="program-info">
                                <h4>My life
                                    <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                                        Journey to within
                                    </p>
                                </h4>
                                <p>It is a six day residential program which unfold itself in the foot hills of Himalya's amidst the abundance of nature. A program exclusively for Chrysalians who have completed the Life Leadership Program successfully.</p>
                                <p>It is a perfect hide away to spend quality time with someone who matters the most – “ YOUR OWN SELF”</p>
                                <p>Since several years, the Saints have been saying that the journey inside, is equal to the journey outside. When we touch our inner self, we have already got the glimpse of the universe outside. But, the irony is that we often search for peace and our happiness outside, when the truth is that everything lies inside. My Life is an upgrade to the Life Leadership Program. It uncovers the layers of pretences we have created around our seed and unveils the most beautiful being in the universe called LOVE. The valleys are calling out to you. Come and experience your My Life.</p>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="tl">
                            <div class="program-info">
                                <h4>Ehsaas
                                    <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                                        Discover the joy of blissful married life
                                    </p>
                                </h4>
                                <p>Marriage…</p>
                                <p>The most intimate & beautiful relationship of life… A fulfilling marriage takes time & effort, it has to be nurtured to strengthen it further… We all have commitments at work, with family and towards life… And quest of fulfilling them, shadows the better half of our lives…
                                Do we ever realize this??</p>
                                <p>Ehsaas will take you on a  a journey full of love, romance, intimacy, togetherness, nostalgia spiced with a lot of fun… EHSAAS is for the couples who are happily married & want to make the relationship happier “It is not about fixing something wrong, but about building it right & strong”
                                </p>

                                <h6>EHSAAS will enable the couples to:</h6>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> Rediscover the essence of their marriage</li>
                                        <li><i class="fa fa-circle"></i> Standby & for each other... Forever</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> Fulfill the aspiration of being the perfect couple</li>
                                        <li><i class="fa fa-circle"></i> Live together the joys & sorrows of the enduring relationship</li>
                                    </ul>
                                </div>
                            </div>                            
                        </div>
                    </div><!-- /end my tab content -->
                </div><!-- /contact_tab -->
            </div>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uprog-lnk">
                <p>To know about our upcoming program <a href="<?php echo $eventPath; ?>events-list/">click here</a></p>
            </div>
        </div>
    </div>
</div>
<div class="container-full testimonial-slider chrysalis-programes">
    <div class="container">
        <div class="row">
            <a href="testimonials.php" class="more-testimonials">Read more testimonials</a>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="slider">
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/testimonials/Nilesh-Maniyar.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            People do not know loving yourself is good. Even I didn't know until Life Leadership program happened to me. Cheers Nilesh!!
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Nilesh Maniyar</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/testimonials/Unmesh-Gugale.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            My life was a journey within, where i could connect to my innersoul and realised the purpose of my life. I could understand the balance that i wanted to have in my life & my journey has begain to achive & maintain it.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Unmesh Gugale</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/testimonials/Jyoti-&-Ganesh-Nimhan.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            Ehsaas has been like a “cherry on the cake “.. has made our fights sweeter & has enabled us to understand each other in a better manner. Enjoyed the experience of writing the love letter the most.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Jyoti & Ganesh Nimhan</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-full padding-top-bottom">
    <div class="row row-no-padding">
        <div class="gallery-slider">
            <a class="item" href="images/individual/ehsaas-2.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/ehsaas-2-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/ehsaas-3.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/ehsaas-3-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/ehsaas-4.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/ehsaas-4-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/llp-1.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/llp-1-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/llp-2.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/llp-2-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/llp-3.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/llp-3-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/mylife-1.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/mylife-1-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/mylife-2.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/mylife-2-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/individual/mylife-3.jpg" rel="prettyPhoto[individual]">
                <img class="item img-responsive" src="images/individual/mylife-3-thumb.jpg" alt="title" />
            </a>
        </div>
    </div>
</div>
<?php include 'footer.php';?>