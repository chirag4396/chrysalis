<div class="header-top navbar-fixed-top">
    <div class="container-full">
        <div class="header-inner-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="row">
                            <div class="hidden-md hidden-lg side-nav">
                                <i class="fa fa-bars" id="side_nav"></i>
                            </div>
                            <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 logo">
                                <a href="<?php echo $chrysalisPath; ?>">
                                    <img src="images/logo.png" title="Chrysalis" class="img-responsive">
                                </a>
                            </div>
                            <div class="hide-1024 col-lg-10 col-md-10 col-ms-10 left-border hidden-sm hidden-xs">
                                <ul class="navigation">
                                    <li id="home"><a href="<?php echo $chrysalisPath; ?>index.php"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                                    <li id="aboutus">
                                        <a href="<?php echo $chrysalisPath; ?>aboutus.php">about us</a>
                                         <ol>
                                            <li><a href="<?php echo $chrysalisPath; ?>aboutus.php">About Chrysalis</a> </li>
                                            <li><a href="<?php echo $chrysalisPath; ?>cef.php">CEF</a></li>
                                        </ol>
                                        <!-- <ol>
                                            <li><a href="<?php echo $chrysalisPath; ?>aboutus.php">About Chrysalis</a></li>
                                            <li><a href="<?php echo $chrysalisPath; ?>team.php">team Chrysalis</a></li>
                                            <li><a href="<?php echo $chrysalisPath; ?>tcf.php">TCF</a></li>
                                            <li><a href="<?php echo $chrysalisPath; ?>cef.php">CEF</a></li>
                                                >>>>>>> origin
                                        </ol>-->
                                    </li>
                                    <li id="chry-programs">
                                        <a href="<?php echo $chrysalisPath; ?>businessOfferings.php">program</a>
                                        <ol>
                                            <li><a href="<?php echo $chrysalisPath; ?>businessOfferings.php">Business growth programs</a>
                                                <ol id = "menuTab">
                                                    <li><a href = "javascript:;" data-href="tgob">The Game Of Business</a></li>
                                                    <li><a href = "javascript:;" data-href="pankh">PANKH</a></li>
                                                    <li><a href = "javascript:;" data-href="tl">Transformation Leadership</a></li>
                                                    <li><a href = "javascript:;" data-href="nvision">N-Vision</a></li>
                                                    <li><a href = "javascript:;" data-href="alygn">Alygn</a></li>
                                                </ol>                                                
                                            </li>
                                            <li><a href="<?php echo $chrysalisPath; ?>individualProgram.php"> Life transformational programs</a>
                                                <ol>
                                                    <li  id="mymenu"><a href="<?php echo $chrysalisPath; ?>aboutus2.php#tab3">LLP</a></li>
                                                    <li><a href="<?php echo $chrysalisPath; ?>businessOfferings.php#ml">My Life</a></li>
                                                     <li><a href="<?php echo $chrysalisPath; ?>businessOfferings.php#ehsaas">Ehsaas</a></li>
                                                </ol>
                                            </li>
                                        </ol>
                                    </li>
                                    <li><a target="_blank" href="http://www.manishgupta.co.in/mg-as-keynote-speaker/">Invite MG</a></li>
                                    <li><a href="<?php echo $eventPath; ?>">Get Inspired</a>
                                         <ol>
                                            <li><a href="<?php echo $chrysalisPath; ?>aboutus.php">Blogs</a> </li>
                                            <li><a href="<?php echo $chrysalisPath; ?>quotes_by_MG.php">Quotes by MG</a></li>
                                            <li><a href="<?php echo $chrysalisPath; ?>videos_by_MG.php">Videos by MG</a></li>
                                        </ol>
                                    </li>
                                    <li><a href="http://www.manishgupta.co.in/" target="_blank">
                                    About MG</a></li>
                                      <li id="clients"><a href="<?php echo $chrysalisPath; ?>clients.php">Our clients</a>
                                    <ol>
                                    <li id="clients"><a href="<?php echo $chrysalisPath; ?>clients.php">clients</a></li>
                                    <li id="testimonials"><a href="<?php echo $chrysalisPath; ?>testimonials.php">Testimonials</a></li>
                                    </ol>
                                      </li>
                                    <li><a href="<?php echo $eventPath; ?>career/">careers</a></li>
                                    <li><a href="<?php echo $eventPath; ?>events-list/">events</a></li>
                                    <li id="contactus"><a href="<?php echo $chrysalisPath; ?>contactus.php">contact</a></li>
                                    <!-- <li><a href="https://mgonline.mykajabi.com/" target="_blank">Get Trained Online</a></li> -->
                                    <!-- <li><a href="javascript:void(0);">shop</a></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

  </div>
</div>

       
    </div>


</div>
</div>


