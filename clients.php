<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    <section class="inner-banner-background">
        <img src="images/1920x534/clients.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/clients.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/clients.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i> -->
            Our clientele<br>
        </p>
        <div class="bottom-line"></div>
    </div>
</div>
<div class="container-full client-container padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Jagtap Horticulture<br>
                        Pvt ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Kumar World</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Amit Enterprises</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Excellence Shelters<br>Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Remonesa</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Suroj Buildcon</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Jalan Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Suvijay Buildcon</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>D.R.Gavhane Landmark</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Ranjeet Developers</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>DNV Builders</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Venkatesh Oxy Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Zenith Landmarks</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Surya Developers</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>K-Pra Foods Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Rainbow Constructions</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Vastushodh</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Elite landmarks</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>ABC properties</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Nandan Buildcon</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Vasudha Developers</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>CK Group,Life Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Kamal Raj Properties</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>G.A.Bhilare Conultants PVT.LTD.</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Kedar Vanjape Developers</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Shree Ram Buliders</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Khandwala Securities Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>AIM Filtertech Pvt Ltd.</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Dran Eng Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Techno Trak Engineers</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Jai Bhawani Mata Stamping</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Amod Industries</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Deepesh Pressing</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Analogic Automation Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Prompt Services</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Cosmos Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Cee Dee Vacuum Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Maharashtra Fastenrs Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Chemdisk Process Solutions</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Mask Polymers Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>V-Smart Thermotech Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Rochi Engineers Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Shree Sidhivinayak</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Poychem</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Elcon</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Siddhi Stamping  & Electronics</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>SSV Engineering</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Fine Handling and Automation pvt ltd.</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Unique circle engg pvt ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Victory Precisions Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Hindustan Ferro Alloy industries</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>TexCarp Consulting</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Vidyut Controls</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Eureka Doors</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Horizon Industries Pvt Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Bake Lite Pure Veg</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Crystal International</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Kakade Laser</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Industrial Packers</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Nissar</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Enertech Ups</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>E.G. Kantawala</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Friction Welding Technologies Pvt. Ltd</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Kundan Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Maharashtra Hosiery</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Rashtriya Khadi Bhandar</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Piebee International</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Gehna Jewels</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Darshan Indani Architect</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Greenlounge</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Sacred Geometry</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Avinash Nawathe Architects</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Beautiful Living</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Suryadatta Intitie of Mgmt</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Diplomat Electricals</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Orbital</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Lexagent</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Dr. Ithape Hospital</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Indus Healthcare</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Shakun Clinc Childerns Hospital</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Wadeshwar</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Hotel Shreyas</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Vyom Labs</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Raghoji Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Konark Advertising & Marketing</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Print Express</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Planedge Consultants</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Ashiyana Real Estate</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Blackgrapeholidays</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Kesari</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Saarrthi Group</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Pride Purple</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>B U Bhandari Landmarks</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Mantri Constructions</p>
                        <i class="fa fa-plus"></i>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 client">
                        <p>Meine Kuiche</p>
                        <i class="fa fa-plus"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>