<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
   <!--  <section class="inner-banner-background">
        <img src="images/1920x534/contact.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/contact.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/contact.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i> -->
            Let's get connected<br>
        </p>
        <div class="bottom-line"></div>
    <!-- </div> --> -->
</div>
<div class="container-full padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-8 col-md-10 col-sm-12 contact-us-txt">
                <p>Team Chrysalis is ready to help you with all you need from us 
with a clear intention of taking you and your business to the next level. 
You can also tell us how we're doing because <strong>every feedback matters.</strong></p>
            </div>
        </div>
    </div>
</div>



<!-- contact -->
<div class="contact" id="contact" style="background-color: #fff!important;">
    
    <div class="w3layouts-grids">
        <div data-aos="flip-left" class="col-md-6 contact-left">
            <h3 data-aos="zoom-in" >Contact information</h3>
            <div class="contact-info">
                <div class="contact-info-left">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
                <div class="contact-info-right">
                    <h5>Address</h5>  
                    <ul>
                        <li>Chrysalis<br>
                        1, Beena, Chrysalis Corner, Senapati Bapat Road, Pune, 411016, Maharashtra, India.</li>
                    </ul>                 
                   
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="contact-info">
                <div class="contact-info-left">
                    <i class="fa fa-mobile" aria-hidden="true"></i>
                </div>
                <div class="contact-info-right">
                    <h5>Contact</h5>
                    <ul>
                        <li>020-25676987 </li>
                        <li>+91-823-700-8400 </li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>



            <div class="contact-info">
                <div class="contact-info-left">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </div>
                <div class="contact-info-right">
                    <h5>E-Mail</h5>
                    <ul>
                        <li><a href="mailto:example@mail.com">23ajaypawar@gmail.com</a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <div data-aos="flip-right" class="col-md-6 contact-form">
            <form class="contact-us-form validate" method="post" name="contactform" id="contactform" role="form" autocomplete="on">
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <input type="text" class="form-control required" name="name" id="name" placeholder="Name">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <input type="email" class="form-control required" name="email" id="email" placeholder="Email">
                        </div>
                        <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <input type="phone" class="form-control required" name="phone" id="phone" placeholder="Contact" minlength="10" maxlength="12">
                        </div>
                       <div class="form-group col-lg-6 col-md-6 col-sm-12">
                            <input type="text" class="form-control required" name="name" id="subject" placeholder="Subject">
                        </div>  
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <textarea onfocus="$(this).val('');" onblur="$(this).val('Message');" class="form-control required" name="message" id="message" rows="5">Message</textarea>
                            <!-- <textarea class="form-control required" name="message" id="message" rows="5">Message</textarea> -->
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <button type="submit" id="contactForm_submit" class="btn btn-default-reverse center-block">Send</button>
                        </div>
                    </form>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //contact -->



<div class="container-full">
    <div class="row row-no-padding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="map" class="">
            <!-- <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lets-connect-container">
                        <div class="lets-connect">
                             <h4>Lets get connected</h4> 
                             <p>Team Chrysalis is ready to help you with all you need from us with a clear intention of taking you and your business to the next level. You can also tell us how we�re doing because every feedback matters.
                            </p>
                        </div>
                    </div>
                </div> 
            </div>-->
                
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7565.571131536482!2d73.8181860396423!3d18.53858990204893!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1371870497d69c7e!2sChrysalis+Business+Solutions+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1462625235868" width="100%" height="458" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div> 
<div class="clearfix"></div>
<?php include 'footer.php';?>