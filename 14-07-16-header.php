<div class="header-top navbar-fixed-top">
    <div class="container-full social-media-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-4 hidden-sm hidden-xs pull-right">
                    <ul class="social-media">
                        <li><a href="https://www.instagram.com/mg_chrysalis" target="_blank"><i class="fa fa-instagram"></i> </a></li>
                        <li><a href="https://www.youtube.com/channel/UCeXq7ztkYXtkLB-27LrdCTQ" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="https://twitter.com/Mgchrysalis" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.facebook.com/Chrysalis-Empowers-1712411335714165/?fref=ts" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-full">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="hidden-md hidden-lg side-nav">
                            <i class="fa fa-bars" id="side_nav"></i>
                        </div>
                        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12 logo">
                            <a href="<?php echo $chrysalisPath;?>">
                                <img src="images/logo.png" title="Chrysalis" class="img-responsive">
                            </a>
                        </div>
                        <div class="hide-1024 col-lg-10 col-md-10 col-ms-10 left-border hidden-sm hidden-xs">
                            <ul class="navigation">
                                <li id="home"><a href="<?php echo $chrysalisPath;?>index.php"><i class="fa fa-home" aria-hidden="true"></i></a></li>
                                <li id="aboutus">
                                    <a href="<?php echo $chrysalisPath;?>aboutus.php">about us</a>
                                    <ol>
                                        <li><a href="<?php echo $chrysalisPath;?>aboutus.php">About Chrysalis</a></li>
                                        <li><a href="<?php echo $chrysalisPath;?>team.php">team Chrysalis</a></li>
                                        <li><a href="<?php echo $chrysalisPath;?>tcf.php">TCF</a></li>
                                        <li><a href="<?php echo $chrysalisPath;?>cef.php">CEF</a></li>
                                    </ol>
                                </li>
                                <li id="chry-programs">
                                    <a href="<?php echo $chrysalisPath;?>businessOfferings.php">program</a>
                                    <ol>
                                        <li><a href="<?php echo $chrysalisPath;?>businessOfferings.php">Business Offerings</a></li>
                                        <li><a href="<?php echo $chrysalisPath;?>individualProgram.php">Individual Programs</a></li>
                                        <li><a target="_blank" href="http://www.manishgupta.co.in/mg-as-keynote-speaker/">Invite MG as a Key Note Speaker</a></li>
                                    </ol>
                                </li>
                                <li><a href="<?php echo $eventPath;?>">life at chrysalis</a></li>
                                <li><a href="http://www.manishgupta.co.in/" target="_blank">Blog</a></li>
                                <li id="clients"><a href="<?php echo $chrysalisPath;?>clients.php">clients</a></li>
                                <li><a href="<?php echo $eventPath;?>career/">careers</a></li>
                                <li><a href="<?php echo $eventPath;?>events-list/">events</a></li>
                                <li id="testimonials"><a href="<?php echo $chrysalisPath;?>testimonials.php">Testimonials</a></li>
                                <li id="contactus"><a href="<?php echo $chrysalisPath;?>contactus.php">contact</a></li>
                                <li><a href="http://mgonline.mykajabi.com/login" target="_blank">Get Trained Online</a></li>
                                <!-- <li><a href="javascript:void(0);">shop</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 