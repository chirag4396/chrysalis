<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<!-- Modal -->
<div class="modal fade subscribe-login" id="subscribe-login" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

            <div class="modal-body">

                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                <h4 class="modal-title">Subscribe for Newsletter</h4>
                <div class="form-respond text-center">
                    
                </div>
                <form class="subscribe-emailer-form validate" method="post" name="subscribe_form" id="subscribe_form" role="form" autocomplete="on">
                    <fieldset class="form-group">
                        <input type="text" class="form-control required" name="name" id="name" placeholder="NAME">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="email" class="form-control required" name="email" id="email" placeholder="EMAIL*">
                    </fieldset>
                    <fieldset class="form-group">
                        <input type="text" class="form-control required" name="phone" id="phone" placeholder="CONTACT">
                    </fieldset>
                    <button type="submit" class="btn subscribe-btn" id="subscribeForm_submit">Submit</button>
                </form>

            </div>
        </div>

    </div>
</div>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    <section class="video-background">
        <div class="homepage-hero-module">
            <div class="video-container">
                <div class="filter"></div>
                <video autoplay loop muted class="fillWidth">
                    <source src="video/Working-Space.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                    <source src="video/Working-Space.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>
                <div class="poster hidden">
                    <img src="video/Working-Space.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <div class="banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i><br> -->
            Are you running your <strong>Business</strong> or <br>
            is your <strong>Business</strong> running you?<br>
            <!-- <span>
                “There’s nothing in a caterpillar that tells you that it is going to be a butterfly”
            </span> -->
            <!-- <div class="play-banner-video">
                <a href="https://www.youtube.com/channel/UCeXq7ztkYXtkLB-27LrdCTQ" target="_blank"><i class="fa fa-play"></i></a>
            </div> -->
        </p>
        <button type="button" class="btn btn-info subscribe-newsletter-button margin-top-20" data-toggle="modal" data-target="#subscribe-login">subscribe for newsletter</button>
        
    </div>
</div>
<div class="container our-programs">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h4 class="section-title">OUR PROGRAMS</h4>
            <p class="title-qoute">
                "If feeding your body is your responsibility then feeding your mind is whose responsibility?" 
                <br>- MG
            </p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="programs-wrapper">
                <div class="col-lg-6 col-md-6 col-sm-12 no-padding prog-height-fix"><img class="img-responsive" src="images/entrepreneurPrograms.jpg"></div>
                <div class="col-lg-6 col-md-6 col-sm-12  our-programs-bg">
                    <h5 class="sub-title">Business&nbsp;<span>offerings</span></h5>
                    <p>The opportunities created by today’s global knowledge economy coupled with the ‘unshackling of indigenous enterprise’, have contributed to making India a ‘fertile ground’ for Entrepreneurship.</p>
                    <a class="btn btn-default" href="businessOfferings.php">Know More</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="programs-wrapper">
                <div class="col-lg-6 col-md-6 col-sm-12 our-programs-bg">
                    <h5 class="sub-title">individual&nbsp;<span>Program</span></h5>
                    <p>At Chrysalis we believe that every individual can be a success story if he continuously works on and expands his success pentagon. This pentagon comprises of knowledge, attitudes, skills, habits and strategies.</p>
                    <a class="btn btn-default" href="individualProgram.php">Know More</a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 no-padding prog-height-fix"><img class="img-responsive" src="images/individualPrograms.jpg"></div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="programs-wrapper">
                <div class="col-lg-6 col-md-6 col-sm-12 hidden-xs no-padding"><img class="img-responsive" src="images/corporatePrograms.jpg"></div>
                <div class="col-lg-6 col-md-6 col-sm-12 our-programs-bg">
                    <h5 class="sub-title">corporate<span>Program</span></h5>
                    <p>In today's highly competitive business world, a committed, efficient, passionate & motivated workforce which is aligned with the organizational goals & objectives gives you the competitive advantage.</p>
                    <a class="btn btn-default" href="javascript:void(0);">Know More</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div> -->
    </div>
</div>
<div class="container-full about-mg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <h6><a href="http://www.manishgupta.co.in/" target="_blank">Meet MG</a></h6>
                        <span>Chairman, Chrysalis Group</span>
                        <p>MG, Chairman, Chrysalis Group. Mentor and role model to a legion, he has sought to serve the aspirations of entrepreneurs, leaders, professionals, homemakers, students and teenagers and thus contributed his mite towards a more evolved society. He is an acknowledged authority on Entrepreneur Empowerment, Leadership Development and Organization Turnaround. He has made a positive difference to the lives of more than half a million, 1000+ Entrepreneurs and 1500+ organizations.
                        <br><br>
                        MG is also the founder of Chrysalis Entrepreneur Forum and The Chrysalis Foundation. 
                        </p>
                        <a class="mg-logo" href="http://www.manishgupta.co.in/mg-as-keynote-speaker/" target="_blank">INVITE MG AS A KEYNOTE SPEAKER</a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 hidden-xs hidden-sm about-mg-img-box">
                        <img class="img-responsive" src="images/aboutMgImg.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container about-chrysalis">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="you-tube-video">
                        <a href="https://www.youtube.com/channel/UCeXq7ztkYXtkLB-27LrdCTQ" target="_blank"><img class="img-responsive" src="images/youtubeChannel.jpg">
                        <!-- <div class="youtube-play-btn">PLAY Videos </div> -->
                        <!-- <img src="images/videoTrangle.png"> -->
                        </a>
                    </div>
                    <h5 class="sub-title">Youtube&nbsp;<span>Channel</span></h5>
                    <p>Listen to MG on our Youtube Channel and stay motivated and inspired on by daily dose of motivation.</p>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="about-info-box">
                        <h5 class="sub-title">About&nbsp;<span>chrysalis</span></h5>
                        <p>Chrysalis is into the business of "<strong>Empowering Entrepreneurs</strong>” through its core activities of business coaching, business consulting and organizational turnaround interventions.<br><br>
                        Chrysalis was founded by MG in 1997 with a very powerful purpose which was "to enable individuals and organizations reach a higher level of thinking thereby giving a meaning to their existence."</p>
                        <a class="" href="aboutus.php">+</a>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full testimonial-slider">
    <div class="container">
        <div class="row">
            <a href="testimonials.php" class="more-testimonials">Read more testimonials</a>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="slider">
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/testimonials/Yatin-Tambe.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            TGOB brought a lot of alignment within the team and me, which improved communication, efficiency, productivity and performance. Effective delegation made people take ownership and accountability. I started getting lot of time for myself. In the last two years, I did “Pune to Kanyakumari” on cycle and “Kanyakumari to Kashmir” on bike. I also travelled from Pune to Singapore on bike which was a distance of 10,000 kms over a period of one and half months.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Mr. Yatin Tambe | (Chairman - Friction Welding Technologies Pvt. Ltd.)</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<?php include 'footer.php';?>