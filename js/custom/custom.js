$(window).load(function() {
    "use strict";
    if ($("#contactform").length) {
        $(".validate").validate();
        var e = $("#contactform");
        var t = $("#contactForm_submit");
        var n = $(".form-respond");
        $(document).on("submit", "#contactform", function(r) {
            r.preventDefault();
            $.ajax({
                url: "includes/sendemail.php",
                type: "POST",
                dataType: "html",
                data: e.serialize(),
                beforeSend: function() {
                    n.fadeOut();
                    t.html("Sending....")
                },
                success: function(data) {
                    e.fadeOut(300);
                    n.html('Thank you...').fadeIn(300);
                    setTimeout(function() {
                        n.html('').fadeOut(300);
                        $("#name, #email,#reason #message").val("");
                        e.fadeIn(1800)
                        t.html('Submit');
                    }, 4e3)
                },
                error: function(e) {
                    console.log(e)
                }
            })
        })
    }
    if ($("#subscribe_form").length) {
        $("#subscribe_form.validate").validate();
        var e = $("#subscribe_form");
        var t = $("#subscribeForm_submit");
        var n = $(".form-respond");
        $(document).on("submit", "#subscribe_form", function(r) {
            r.preventDefault();
            $.ajax({
                url: "includes/process.php",
                type: "POST",
                dataType: "html",
                data: e.serialize(),
                beforeSend: function() {
                    console.log('in beforeSend');
                    n.fadeOut();
                    t.html("Sending....")
                },
                success: function(data) {
                    console.log('in success');
                    e.fadeOut(300);
                    n.html('Subscribe sccessfully!<br/>Thank you...').fadeIn(300);
                    setTimeout(function() {
                        n.html('').fadeOut(300);
                        $("#name, #email, #phone", "#reason").val("");
                        e.fadeIn(1800);
                        $('#subscribe-login').modal('hide');
                        t.html('Submit');
                    }, 4e3)
                },
                error: function(e) {
                    console.log('in error');
                    console.log(e)
                }
            })
        })
    }
});
(function($) {
    $('.subscribe-newsletter-button-new').click(function(){
        // $('.close').hide();
        // $('.modal-title').text('Subscribe for our Program');
    });
    $('.subscribe-newsletter-button').click(function(){
        // $('.close').show();
        // $('.modal-title').text('Subscribe for Newsletter');
    });
    setTimeout(function(){
        $('.subscribe-newsletter-button-new').trigger('click');
    },10000);
    //jQuery is required to run this code
    $(document).ready(function() {
        // console.log("currentPage==>",currentPage);
        $('ul.navigation').find('li#'+currentPage+' a').addClass('active');
        if(!($(window).width() <= 768)){
            $(window).scroll(function() {    
                var scroll = $(window).scrollTop();
                if (scroll >= 100) {
                    $("body").addClass("smallHeader");
                } else {
                    $("body").removeClass("smallHeader");
                }
            });
        }
        $("a[rel^='prettyPhoto']").prettyPhoto({
            slideshow:5000,
            social_tools: false,
            autoplay_slideshow:true
        });
        $(".slider").owlCarousel({
            // autoPlay: 3000, //Set AutoPlay to 3 seconds
            navigation : true,
            items: 1,
            itemsDesktop: [1199, 1],
            itemsDesktopSmall: [979, 1],
            itemsTablet : [768, 1],
            itemsMobile : [479, 1]
        });
        $(".gallery-slider").owlCarousel({
            // autoPlay: 3000, //Set AutoPlay to 3 seconds
            navigation : true,
            items: 4,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3]
        });
        var $container = $('.testi-container');
        $('.program-info:first').show();
        $(".programs-tab ul li a").click(function(e) {
            e.preventDefault();
            var divId = $(this).attr('href');
            $('.program-info').hide();
            $('.programs-tab ul li a').removeClass('active');
            $(this).addClass('active');
            $(divId).fadeIn(500);
        });
        if($(window).width() <= 768){
            $('.team-member div.row').isotope();
        }
        $(".testimonial-filter ul li").click(function() {
            console.log("auto trigger");
            var id = $(this).attr('id');
            $container.isotope({
                filter: '.'+id
            });
            $(".testimonial-filter ul li").removeClass('active');
            $(this).addClass('active');
        });
        $( ".testimonial-filter ul li:first" ).trigger( "click" );
        $("#side_nav").click(function(e) {
            e.preventDefault();
            $("body").toggleClass("toggled");
        });

        scaleVideoContainer();
        initBannerVideoSize('.video-container .poster img');
        initBannerVideoSize('.video-container .filter');
        initBannerVideoSize('.video-container video');
        $(window).on('resize', function() {
            scaleVideoContainer();
            scaleBannerVideoSize('.video-container .poster img');
            scaleBannerVideoSize('.video-container .filter');
            scaleBannerVideoSize('.video-container video');
        });
    });

    function scaleVideoContainer() {
        var height = $(window).height() + 5;
        var unitHeight = parseInt(height) + 'px';
        $('.homepage-hero-module').css('height', unitHeight);
    }

    function initBannerVideoSize(element) {
        $(element).each(function() {
            $(this).data('height', $(this).height());
            $(this).data('width', $(this).width());
        });
        scaleBannerVideoSize(element);
    }

    function scaleBannerVideoSize(element) {
        var windowWidth = $(window).width(),
            windowHeight = $(window).height() + 5,
            videoWidth,
            videoHeight;
        console.log(windowHeight);
        $(element).each(function() {
            var videoAspectRatio = $(this).data('height') / $(this).data('width');
            $(this).width(windowWidth);
            if (windowWidth < 1000) {
                videoHeight = windowHeight;
                videoWidth = videoHeight / videoAspectRatio;
                $(this).css({
                    'margin-top': 0,
                    'margin-left': -(videoWidth - windowWidth) / 2 + 'px'
                });
                $(this).width(videoWidth).height(videoHeight);
            }
            $('.homepage-hero-module .video-container video').addClass('fadeIn animated');
        });
    }
})(jQuery);
