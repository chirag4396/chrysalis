<!-- Sidebar -->
        <div id="sidebar-wrapper" class="hidden-md hidden-lg">
            <ul class="sidebar-nav">
                <li><a class="active" href="<?php echo $chrysalisPath;?>index.php">Home</a></li>
                <li>
                    <a href="<?php echo $chrysalisPath;?>aboutus.php">about us</a>
                    <ol>
                        <li><a href="<?php echo $chrysalisPath;?>aboutus.php">About Chrysalis</a></li>
                        <li><a href="<?php echo $chrysalisPath;?>team.php">team Chrysalis</a></li>
                        <li><a href="<?php echo $chrysalisPath;?>tcf.php">TCF</a></li>
                        <li><a href="<?php echo $chrysalisPath;?>cef.php">CEF</a></li>
                    </ol>
                </li>
                <li>
                    <a href="<?php echo $chrysalisPath;?>businessOfferings.php">program</a>
                    <ol>
                        <li><a href="<?php echo $chrysalisPath;?>businessOfferings.php">Business Offerings</a></li>
                        <li><a href="<?php echo $chrysalisPath;?>individualProgram.php">Individual Programs</a></li>
                        <li><a target="_blank" href="http://www.manishgupta.co.in/mg-as-keynote-speaker/">Invite MG as a Key Note Speaker</a></li>
                    </ol>
                </li>
                <li><a href="<?php echo $eventPath;?>">Blog by chrysalis</a></li>
                <li><a href="http://www.manishgupta.co.in/" target="_blank">About MG</a></li>
                <li id="clients"><a href="<?php echo $chrysalisPath;?>clients.php">clients</a></li>
                <li><a href="<?php echo $eventPath;?>career/">careers</a></li>
                <li><a href="<?php echo $eventPath;?>events-list/">events</a></li>
                <li id="testimonials"><a href="<?php echo $chrysalisPath;?>testimonials.php">Testimonials</a></li>
                <li id="contactus"><a href="<?php echo $chrysalisPath;?>contactus.php">contact</a></li>
                <!-- <li><a href="http://mgonline.mykajabi.com/login" target="_blank">Get Trained Online</a></li> -->
                <!-- <li><a href="javascript:void(0);">shop</a></li> -->
            </ul>
        </div>