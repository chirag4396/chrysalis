<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
</div>


<p>
    <button onclick="autoscroll()"> </button>
</p>
<div id="sticky-anchor"></div>
<div id="sticky"></div>

<div class="padding-top2">
    <ul id="myTab" class="container text-center nav nav-tabs navbar-fixed-top2" role="tablist">
        <li class="active" id = "tgob-li"><a href="#tgob" role="tab" data-toggle="tab">TGOB</a></li>
        <li id = "pankh-li"><a href="#pankh" role="tab" data-toggle="tab">PANKH</a></li>
        <li id = "tl-li"><a href="#tl" role="tab" data-toggle="tab">TL</a></li>
        <li id = "nvision-li"><a href="#nvision" role="tab" data-toggle="tab">N-VISION</a></li>
        <li id = "alygn-li"><a href="#alygn" role="tab" data-toggle="tab">ALYGN</a></li>
    </ul>
</div>

<div class="container-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <div class="contact_tab myspace">
                    
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="tgob">
                            <div class="container">
                                <div class="program-info">
                                    <h4>The Game Of Business
                                        <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                                            An Entrepreneur Empowerment Program
                                        </p>
                                    </h4>
                                    <p>The Game Of Business is an initiative by Chrysalis towards empowering entrepreneurs and providing them with practical business education, coaching, advising and networking. Through the program, participants will gain insight and skills in topics such as entrepreneurial mindset, vision planning, values & purpose identification, sales & marketing, finance, leadership & human resource optimization by proper employee management and all this is supported by a very strong implementation in their businesses.</p>
                                    <p>Designed for successful established entrepreneurs, the program provides what is needed to break through current challenges, the causes of which could vary right from the economy, industry related challenges, individual circumstances or habits.</p>
                                    <p>This program is a 36 session intervention with 15 contact/ knowledge sessions, 10 one-on-one Reviews, 10 group reviews and 1 Master review.</p>
                                    <h6>Deliverables</h6>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul>
                                            <li><i class="fa fa-circle"></i> What it takes to be an entrepreneur</li>
                                            <li><i class="fa fa-circle"></i> Pathways to building a visionary organization</li>
                                            <li><i class="fa fa-circle"></i> Mastering the strategic planning</li>
                                            <li><i class="fa fa-circle"></i> Organizational design and human resource optimization</li>
                                            <li><i class="fa fa-circle"></i> Building an effective & efficient second line of command</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <ul>
                                            <li><i class="fa fa-circle"></i> Effective leadership and delegation strategies</li>
                                            <li><i class="fa fa-circle"></i> Bringing rhythm in the organization to create an empowering culture</li>
                                            <li><i class="fa fa-circle"></i> Setting up systems & processes</li>
                                            <li><i class="fa fa-circle"></i> Sales, marketing and branding strategies</li>
                                            <li><i class="fa fa-circle"></i> Effective financial planning and wealth management</li>
                                        </ul>
                                    </div>
                                </div>
                            </div> <!-- End Container -->
                        </div><!-- End Tab Pane -->

                        <div class="tab-pane fade" id="pankh">
                            <div class="program-info">
                                <h4>Pankh - Empowering Wopreneurs
                                    <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                                        A Business Program for Wopreneurs (Women Entrepreneurs)
                                    </p>
                                </h4>
                                <p>Pankh is an initiative by Chrysalis towards empowering Wopreneurs (Women Entrepreneurs) and provides women entrepreneurs with practical business education, coaching, advising and networking. Through the program, participants will gain insight and skills in topics such as entrepreneurial mindset, vision planning, values & purpose identification, sales & marketing, finance, business & social etiquette and employee management and is supported by implementation in their businesses.</p>
                                <p>The world needs women entrepreneurs, and women entrepreneurs need all of us.</p>
                                <p>Pankh is a 32 session intervention with 12 contact/knowledge sessions, 10 implementation sessions and 10 peer review sessions. It is specifically designed for existing and aspiring Wopreneurs to help them identify and maximize their potential.
                                </p>
                                <h6>Deliverables</h6>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> The power of a woman and what it takes<br> to be a woman entrepreneur (Wopreneur)</li>
                                        <li><i class="fa fa-circle"></i> Identifying your unique ability</li>
                                        <li><i class="fa fa-circle"></i> Build a visionary organization</li>
                                        <li><i class="fa fa-circle"></i> Mastering the strategic planning</li>
                                        <li><i class="fa fa-circle"></i> Organizational design and human resource optimization</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> Effective leadership and delegation strategies</li>
                                        <li><i class="fa fa-circle"></i> Understanding of sales &amp; marketing process</li>
                                        <li><i class="fa fa-circle"></i> Understanding of financial best practices</li>
                                        <li><i class="fa fa-circle"></i> Business Communication and Style &amp; Elan</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="tl">
                            <div class="program-info">
                                <h4>Transformation Leadership</h4>
                                <p>This is a 2 day nonresidential program aimed at helping the leaders to overcome the challenges they are facing in today's fast moving world. The program focuses on getting work done out of people, retaining them in your organization for a long time, keeping the motivation and productivity high of subordinates. Influencing team members and subordinates to work or behave in sync with the companies' vision and mission.</p>
                                <p>An Entrepreneur always invests in his belief and one of the best investments an entrepreneur can do is to invest in the development of his team.</p>
                                <h6>Deliverables</h6>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> Decision Making</li>
                                        <li><i class="fa fa-circle"></i> Influencing Style & Tactics</li>
                                        <li><i class="fa fa-circle"></i> Situational Leadership</li>
                                        <li><i class="fa fa-circle"></i> Personality Traits</li>
                                        <li><i class="fa fa-circle"></i> Leadership Styles</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> Open Communication</li>
                                        <li><i class="fa fa-circle"></i> Tracking performance of the team</li>
                                        <li><i class="fa fa-circle"></i> Team Alignment</li>
                                        <li><i class="fa fa-circle"></i> Bring in ease of doing business</li>
                                    </ul>
                                </div>
                            </div>                            
                        </div>

                        <div class="tab-pane fade" id="nvision">
                            <div class="program-info">
                                <h4>N-Vision</h4>
                                <p>Only a few of the companies survive from all the millions of companies formed in a year. Sadly, entrepreneurs realise it in a very crucial stage when nothing can be saved. Some businesses vanish after an impressive start. When it comes to failure of a business, then reasons are many but when it comes to growing/flourishing it then reasons are less and very clear.</p>
                                <p>The reason for most of the businesses to survive and then grow is that they have done a better planning. Their purpose is strong enough to drive them in hard times, their vision is big enough to allow them expand on time and again and their values are clear enough to give their customer as well as employees a great experience.</p>
                                <p>Now the question is how to set the vision, how to identify the core values and how to discover what exactly is your organisations’ purpose. Chrysalis presents N-Vision, a two days residential program for entrepreneurs which focuses on-</p>
                                <p><strong>Building a powerful Vision</strong> - You would never board a train without knowing its destination. How can you build your organization not knowing where it is heading?</p>
                                <p><strong>Finding your Core Purpose</strong> - Every organism on this planet has purpose to serve so does your organization. It is just a matter of discovering and working towards it.</p>
                                <p><strong>Discovering your Core Values</strong> - When a storm comes, what happens to the tree with weak roots? It gets uprooted. A strong foundation is indispensible for a lasting future and enduring success</p>
                                <p><strong>Setting Goles</strong> - Working without goals is just working on something or towards nothing. Everybody is born the same but the difference comes in the way you are thought of after you. Our brain is conditioned to process continuously. What you feed to that is what it shall process. Having your goals in front of you opens you up to avenues you never knew existed and in the process you keep moving into the next orbit.</p>
                                <p><strong>Make yourself attractive</strong> - As you grow, you would need more resources, more money and better skills. But without Vision, all this would be nothing but just a hazy view. Only when you know where you want to reach, you shall build resources which will help you achieve your dreams.</p>
                            </div>                            
                        </div>

                        <div class="tab-pane fade" id="alygn">

                            <div class="program-info">
                                <h4>ALYGN
                                <!-- <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                                    Focused Intense Result-oriented Expertise
                                </p> -->
                                </h4>
                                <p>Many organizations focus on training employees to increase sales,  provide better services, setting up processes and retaining the right talent but one thing which is the most important to achieve an organisation's goals is “team alignment”.  If team is not aligned then significant gaps are often seen in team members when it comes to understanding their organisation's top opportunities and top work priorities. A few of the team alignment issues are </p>
                                <h6>Deliverables</h6>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> Less commitments</li>
                                        <li><i class="fa fa-circle"></i> No one takes accountability</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <ul>
                                        <li><i class="fa fa-circle"></i> Fear of conflict</li>
                                        <li><i class="fa fa-circle"></i> Trust issues</li>
                                    </ul>
                                </div>
                                <p class="clearfix"></p>
                                <p>Is your team also suffering from any of the above dysfunction and stopping you from achieving peak performance?  If so, we highly recommend you and your team to join ALYGN by MG.</p>
                                <p><strong>ALYGN</strong> - a one day alignment session on core ideology, vision, strategic sheet with your senior leadership.</p>
                                <p><strong>ALYGN</strong> will provide direction and focus to your team and help them build a plan of action that is line with the organisations goals.</p>
                            </div>
                        </div>

                    </div><!-- /end my tab content -->
                </div><!-- /contact_tab -->
            </div>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">


            <div class="container">
                <div class="row">

                    <!-- Form -->
                    <div class="nb-form">
                        <p class="title text-center">Send Enquiry</p>   

                        <form method="POST" role="form" id="myform">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="" placeholder="Enter Full Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="" placeholder="Enter Email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" id="" placeholder="Enter Phone No.">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="inputMessage" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <center>
                                <input type="submit" name="name" id="submit" value="Submit" class="btn btn-default">
                            </center>
                        </form>
                    </div>
                </div>
            </div>




            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uprog-lnk">

                <p>To know about our upcoming program <a href="<?php echo $eventPath; ?>events-list/">click here</a></p>
            </div>
        </div>
    </div>
</div>
<div class="container-full testimonial-slider chrysalis-programes">
    <div class="container">
        <div class="row">
            <a href="testimonials.php" class="more-testimonials">Read more testimonials</a>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="slider">
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/testimonials/Pravin-Biche.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            TGOB has built in a synergy in the organization. Because of N-Vision what seemed to be an endless effort for me in the past six years was achieved in a single session. I am now in a position to align all my directors with the company's vision and further to share the same ideology.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Mr. Pravin Biche | MD - Orbital</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/testimonials/Anju-Bansal.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            TGOB gave a lot of confidence and validated my beliefs and ideas. The sharings of participants helped in taking care of a lot of challenges. A positive outlook towards business became very strong.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Ms. Anju Bansal | Director - Pie Bee</span>
                    </div>
                    <div class="testimonial-container">
                        <div class="testimonial-user-img">
                            <img src="images/testimonials/Rahul-&-Mrs.-Monika-Dhad.png">
                        </div>
                        <p class="testimonial-txt">
                            <em class="qouteTop"><img src="images/qouteTop.png"></em>
                            We believe that mentorship is very important. A mentor helps set guidelines in life which takes you to the next level of growth. Having a Guru like MG is a blessing. Life has become simpler and the mantra is “No postponement of life”. On the professional level the team has become more aligned and co-operative.
                            <em class="qouteBottom"><img src="images/qouteBottom.png"></em>
                        </p>
                        <span>Mr. Rahul & Mrs. Monika Dhad | Directors - Shivaji Technologies</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full padding-top-bottom">
    <div class="row row-no-padding">
        <div class="gallery-slider">
            <a class="item" href="images/businessOfferings/bo-1.jpg" rel="prettyPhoto[businessOfferings]">
                <img class="item img-responsive" src="images/businessOfferings/bo-1-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/businessOfferings/bo-2.jpg" rel="prettyPhoto[businessOfferings]">
                <img class="item img-responsive" src="images/businessOfferings/bo-2-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/businessOfferings/bo-3.jpg" rel="prettyPhoto[businessOfferings]">
                <img class="item img-responsive" src="images/businessOfferings/bo-3-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/businessOfferings/bo-4.jpg" rel="prettyPhoto[businessOfferings]">
                <img class="item img-responsive" src="images/businessOfferings/bo-4-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/businessOfferings/bo-5.jpg" rel="prettyPhoto[businessOfferings]">
                <img class="item img-responsive" src="images/businessOfferings/bo-5-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/businessOfferings/bo-6.jpg" rel="prettyPhoto[businessOfferings]">
                <img class="item img-responsive" src="images/businessOfferings/bo-6-thumb.jpg" alt="title" />
            </a>
            <a class="item" href="images/businessOfferings/bo-7.jpg" rel="prettyPhoto[businessOfferings]">
                <img class="item img-responsive" src="images/businessOfferings/bo-7-thumb.jpg" alt="title" />
            </a>
        </div>
    </div>
</div>
<?php include 'footer.php';?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $('#result').hide();
    });
    $("#submit").on('click',function(p){
        p.preventDefault();
        var formdata = new FormData($('#myform')[0]);

        $.ajax({
            url:'enquiry.php',
            type:'post',
            data:formdata,
            processData:false,
            contentType:false,
            success:function(res){
                if (res.indexOf('ok')>=0)
                {
                    $('#result').show();
                    $('#myform')[0].reset();
                    window.setTimeout(function(){
                        $('#result').hide('blind');
                    },3000);
                }
            }
        });
    });
    var menu = ['tgob', 'pankh', 'tl', 'nvision', 'alygn'];
    <?php if(isset($_GET['type'])){
        $d = $_GET['type']; 
        ?>
        $.each(menu, function(i,j){            
            $('#'+j).removeClass('active in');
            $('#'+j+'-li').removeClass('active');
            
        });
        $('#'+'<?php echo $d; ?>').addClass('active in');
        $('#'+'<?php echo $d; ?>'+'-li').addClass('active');
        <?php 
    } ?>
    $('#menuTab li a').each(function(k,v){        
        $(v).on({
            'click' : function(){
                var id = $(this).attr('data-href');                
                $.each(menu, function(i,j){
                    if(id != j){
                        $('#'+j).removeClass('active in');
                        $('#'+j+'-li').removeClass('active');
                    }
                });
                $('#'+id).addClass('active in');
                $('#'+id+'-li').addClass('active');                
            }
        });
    });
</script>