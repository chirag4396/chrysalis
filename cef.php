<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    
</div><br>
<div class="container-full" style="margin-top: 60px;">
    <div class="container our-programs cef-w-a-chry">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <h4 class="section-title">Chrysalis Entrepreneur Forum</h4>
                <div class="bottom-line"></div>
            </div>
            <!-- <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <h4 class="section-title">Chrysalis Entrepreneur Forum</h4>
            </div> -->

            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>CEF is a conglomerate of Entrepreneurs which aims to build better businesses and in turn make a positive dent in the GDP of The Nation. It maintains the momentum that is built during "The Game of Business" an entrepreneur empowerment program. The forum consists of core members who provide the growth ecosystem to the </p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <p>different teams of CEF members from different industries, who are ambitious, growth oriented and committed to learn and share from their live experiences and together achieve growth. Single intelligence is always subordinated by cumulative or group intelligence.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <div class="cef-our-purpose padding-top-bottom">
                    <div class="col-lg-6 col-md-6 col-sm-12 pull-right">
                        <h5 class="sub-title">Our&nbsp;<span>Purpose</span></h5>
                        <p>To continuously experience the joy of building<br> better businesses, thereby leading to inclusive<br> growth.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="container-full core-value-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50">
                <h4 class="section-title">Our Core Values</h4>
                <div class="bottom-line"></div>
            </div>
            <div class="col-lg-12 core-value margin-bottom-50">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p>We will Build Success in<br>Others: Success transfers</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p>Abundance Mindset<br><br></p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <i class="fa fa-star"></i>
                        <p>Implementation</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 last-value">
                        <i class="fa fa-star"></i>
                        <p>No Postponement of Life</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!-- services -->
<div class="w3ls-section services" id="services">
    <div class="container">
         <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-50 myttl">
            <h4 class="section-title">Our Core Values</h4>
            <p class="title-qoute col-lg-8 col-md-8 col-sm-8 center-block">
               
            </p>
            <div class="bottom-line"></div>
        </div>
        <div class="services-w3ls-row agileits-w3layouts service1 text-center">
            <p> No free lunch<br>
                No entitlements<br>
                Merit based opportunities</p>  
            <div class="s-top">
                <span class="fa fa-cog" aria-hidden="true"></span>
            </div>

        </div>
        <div class="services-w3ls-row agileits-w3layouts middlegrid-w3ls">
            <div class="col-md-4 services-grid agileits-w3layouts service2">
                <div class="col-md-10 w3ls-sub-text">
                    <p>Organization before self<br>
                            We are solution oriented<br>
                        We have open communication</p>
                </div>  

                <div class="col-md-2 sub-icon">
                    <span class="fa fa-cog" aria-hidden="true"></span>
                </div>             
            </div>
            <div class="col-md-4 services-grid img-agileits">
                <img src="images/services.jpg" class="img-responsive" alt="" />
            </div>
            <div class="col-md-4 services-grid agileits-w3layouts service3">               
                
                <div class="col-md-2 sub-icon">
                    <span class="fa fa-cog" aria-hidden="true"></span>
                </div>

                <div class="col-md-10 w3ls-sub-text">
                     <p> Live, learn, love & leave a legacy<br>
                            We enjoy health & prosperity<br>
                            We have a learning culture<br>
                            We enrich relationships by giving back in abundance<br>
                        We contribute to humanity</p>                                   
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>

        <div class="services-w3ls-row agileits-w3layouts service4">
            <div class="s-top">
                <span class="fa fa-cog" aria-hidden="true"></span>
            </div>
            <p>We strive for excellence</p>
        </div>


    </div>
</div><br>
<!-- //services -->
<div class="clearfix"></div>

<div class="container-full header-container our-vision cef-our-vision padding-top-bottom">

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-15">
                <h4 class="section-title">Our Vision</h4>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <img src="" class="img-responsive">
                </div>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <p>Building a strong fraternity of growth hungry entrepreneurs supporting an<br> inbuilt ecosystem. We intend to make a contribution of 2% to the GDP<br> growth of India by 2025.</p>
                    <div class="bottom-line"></div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1">
                    <img src="" class="img-responsive">
                </div>

            </div>
        </div>
    </div>
</div>
<div class="container-full cefActivitiesInitiatives padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4 class="section-title margin-bottom-40">CEF Activities &amp; Initiatives</h4>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        <p>Apart from continuously upgrading oneself with organizational skills, various life enhancement activities & initiatives are taken in the domain of social, professional and a belief that there is no postponement of life. It is about the right balance between doing work and having fun.</p>
                    </div>
                    <div class="col-sm-6">
                        <strong>Speakers at CEF</strong>
                        <p>CEF invites eminent speakers every month from different walks of life to address the forum members which enables them to scale up their organisations.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full padding-top-bottom">
    <div class="container team-hum">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4 class="section-title">Team HUM</h4>
                <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                    An Organisational Vision Program
                </p>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-40">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="team-hum-desc">A 3 hour power packed session by MG that brings together Entrepreneurs and their team members to realign themselves to the organisational vision and purpose. In this program, MG connects seamlessly with every team member of various organizations and explains the sincere purpose of having a single vision in a context easily understood. The accent here is on how there should be team ownership and accountability by inculcating the core purpose and the core values of the organization.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>