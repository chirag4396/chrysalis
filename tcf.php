<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
    <section class="inner-banner-background">
        <img src="images/1920x534/about.jpg" class="img-responsive hidden-sm hidden-xs">
        <img src="images/768x200/about.jpg" class="img-responsive hidden-md hidden-lg hidden-xs">
        <img src="images/mobile/about.jpg" class="img-responsive hidden-sm hidden-md hidden-lg">
    </section>
    <div class="banner-qoute inner-banner-qoute">
        <p>
            <!-- <i class="fa fa-diamond"></i> -->
            <br>
        </p>
        <div class="bottom-line"></div>
    </div>
</div>

<div class="container-full padding-top-bottom">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="">
                    <div class="col-lg-6 col-md-6 col-sm-12 cf-logo">
                        <img src="images/cfLogo.png" class="img-responsive">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 cf-details">
                        <div class="">
                            <p><strong>TCF</strong> is the corporate social responsibility division of Chrysalis with the strong purpose “to contribute through our mind, body and soul and thereby create an empowered India.”
                            The Foundation has worked towards making a difference in the lives of people in India. It has encouraged people towards a more positive outlook, to take the responsibility to better their lives and the society.</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-full">
    <div class="container initiatives">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 margin-bottom-20">
                <h4 class="section-title">Initiatives</h4>
                <div class="bottom-line"></div>
            </div>
            <br class="clearfix">
            <div class="col-lg-12 margin-bottom-50">
                <?php
                            include("../config.php");
/*
                                $sql = "SELECT * from feedback";
                                $result = mysqli_query($conn, $sql);*/
                                $res = $conn->query('select * from tcf_details');   
                                if($res->num_rows){

                                    ?>
                <ul>
                    <div class="circle top"></div>
                    <div class="circle bottom"></div>
                    <li>
                        <div class="data">
                            <div class="date"><span>From</span>2000</div>
                            <div class="initiative-img">
                                <img src="images/initiative-01.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Association with Army Paraplegic Rehabilitation Centre , khadki, Pune</h5>
                                <p>APRC is the centre for rehabilitation of paraplegic and tetra-plegic soldiers who have been injured on the war front while serving the nation.TCF has  been associated with APRC  since 1999. TCF has been instrumental in generating funds for APRC and has been an integral part of the lives of inmates by being with them for Independence Day, Republic Day and  Ganesh Chaturthi celebrations.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <!-- <li>
                        <div class="data">
                            <div class="date">2010</div>
                            <div class="initiative-img">
                                <img src="images/initiative-02.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Association with Manavya</h5>
                                <p>Manavya is a home for nursing, caring and sheltering orphaned and destitute children and women afflicted by HIV/AIDS. The foundation raised funds for the organisation by organizing power talks titled “I – The Miracle”.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2010</div>
                            <div class="initiative-img">
                                <img src="images/initiative-03.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Roti, Kapda, Muskan<br>
                                (An initiative to collect rice and clothes for the tribals of Velhe)
                                </h5>
                                <p>Manavya is a home for nursing, caring and sheltering orphaned and destitute children and women afflicted by HIV/AIDS. The foundation raised funds for the organisation by organizing power talks titled “I – The Miracle”.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2012</div>
                            <div class="initiative-img">
                                <img src="images/initiative-04.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>Mission Jal Samruddhi <br>
                                (Initiative to desilt a 100-acre lake to provide potable water in Beed)
                                </h5>
                                <p>TCF initiated “Project Jal Samruddhi” to desilt the 100 acre Nizam Era Twarita Devi Pond at Talawade in Beed District by raising funds to the tune of well over Rs. 30 Lakhs for this project. This project resulted in 1500 acres of land becoming fertile and 60 crore liters of water generation for the area.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li>
                    <li>
                        <div class="data">
                            <div class="date">2013</div>
                            <div class="initiative-img">
                                <img src="images/initiative-05.jpg">
                            </div>
                            <div class="initiative-details">
                                <h5>One Day for My City</h5>
                                <p>It is an initiative to encourage the citizens of the city to give one day periodically for the betterment of the city. Some of the activities undertaken under this initiative are cleaning of PMPML buses, Creating awareness among citizens to participate in the electoral process by exercising their right to vote in the Recent Lok Sabha Elections and participating in the Swachh Bharat Abhiyan.</p>
                            </div>
                            <br class="clearfix">
                        </div>
                    </li> -->
                    <!--  -->
                </ul>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>