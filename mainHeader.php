<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="">
        <?php
            $currentPageUrl = basename($_SERVER['PHP_SELF']);
            if(array_search($currentPageUrl, ["businessOfferings.php","individualProgram.php",]) !== false){?>
                <meta name="keyword" content="Leadership Skills Training, life coaching courses, Relationship Training, Workshop for Couples, Program for Couples">
                <meta name="description" content="Chrysalis believes that success is a combination of five elements. To be successful, one needs to continuously expand ones knowledge, create strong attitudes, develop new skills, form good habits and implement result focused strategies.">
                
        <?php }else{ ?>
                <meta name="keyword" content="business coaching, life coaching , executive coaching, personal coaching, leadership coaching, business coaches, coaching and mentoring, life coaching courses.">
                <meta name="description" content="Chrysalis is into the business of 'Empowering Entrepreneurs' through its core activities of business coaching, business consulting and organizational turnaround interventions.">
        <?php } ?>

        <title>Business Coach in Pune | Mentor to the legion | Life Coaching | Business Training | Leadership Training</title>
        <meta property="og:url"           content="http://www.chrysalis.net.in/" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="Chrysalis - Empowering Entrepreneurs" />
        <meta property="og:description"   content="The opportunities created by today’s global knowledge economy coupled with the ‘unshackling of indigenous enterprise’, have contributed to making India a ‘fertile ground’ for Entrepreneurship." />
        <meta property="og:image"         content="http://www.chrysalis.net.in/video/Working-Space.jpg" />


        <!-- Custom Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
        <link href='css/vender/font-awesome.min.css' rel='stylesheet' type='text/css'/>
        <!-- Bootstrap Core CSS -->
        <!-- inject:css -->
        <link href="css/vender/bootstrap.min.css" rel="stylesheet">

        <link href="css/custom/bootstrap-theme.css" rel="stylesheet">
        <link href="css/vender/owl.carousel.css" rel="stylesheet">
        <!-- <link href="css/vender/owl.theme.css" rel="stylesheet"> -->
        <link href="css/vender/prettyPhoto.css" rel="stylesheet">
        <link href="css/custom/video.css" rel="stylesheet">
        <link href="css/custom/custom.css" rel="stylesheet">
        <link href="css/custom/sidebar-nav.css" rel="stylesheet">
        <link href="css/custom/media-query.css" rel="stylesheet">
        <link href="css/custom/style2.css" rel="stylesheet">
  <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen">
        
        <link href="css/custom/aos.css" rel="stylesheet" type="text/css" media="all" /><!-- //animation effects-css-->
        <!-- endinject -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php 
            // print_r($_SERVER['SERVER_NAME']);
            if($_SERVER['SERVER_NAME'] === 'chrysalis'){
                $chrysalisPath = "http://chrysalis/";
                $eventPath = "http://chrysalis/events/";
            }else if($_SERVER['SERVER_NAME'] === 'demo.fivelements.in'){
                $chrysalisPath = "http://demo.fivelements.in/chrysalis/";
                $eventPath = "http://demo.fivelements.in/event/";
            }else if($_SERVER['SERVER_NAME'] === 'www.chrysalis.net.in' || $_SERVER['SERVER_NAME'] === 'chrysalis.net.in'){
                $chrysalisPath = "http://www.chrysalis.net.in/";
                $eventPath = "http://events.chrysalis.net.in/";
            }
            // echo '$chrysalisPath==>'.$chrysalisPath;
            // echo '$eventPath==>'.$eventPath;

            $currentPage="";
            if($currentPageUrl == "index.php"){
                $currentPage = "home";
            }else if(array_search($currentPageUrl, ["businessOfferings.php","individualProgram.php",]) !== false){
                
                $currentPage = "chry-programs";
            }else if(array_search($currentPageUrl, ["aboutus.php","cef.php","tcf.php","team.php"]) !== false){
                $currentPage = "aboutus";
            }else if($currentPageUrl == "testimonials.php"){
                $currentPage = "testimonials";
            }else if($currentPageUrl == "clients.php"){
                $currentPage = "clients";
            }else if($currentPageUrl == "contactus.php"){
                $currentPage = "contactus";
            }

        ?>
        <script type="text/javascript">
            var currentPage = '<?php echo $currentPage; ?>';
        </script>
    </head>

    <body class="toggled">
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1682262455373611');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1682262455373611&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->