<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Chrysalis - Empowering Entrepreneur</title>

        <!-- Custom Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'>
        <link href='css/vender/font-awesome.min.css' rel='stylesheet' type='text/css'/>
        <!-- Bootstrap Core CSS -->
        <!-- inject:css -->
        <link href="css/vender/bootstrap.min.css" rel="stylesheet">

        <link href="css/custom/bootstrap-theme.css" rel="stylesheet">
        <!-- <link href="css/vender/owl.carousel.css" rel="stylesheet"> -->
        <!-- <link href="css/vender/owl.theme.css" rel="stylesheet"> -->
        <!-- <link href="css/vender/prettyPhoto.css" rel="stylesheet"> -->
        <!-- <link href="css/custom/video.css" rel="stylesheet"> -->
        <!-- <link href="css/custom/custom.css" rel="stylesheet"> -->
        <!-- <link href="css/custom/sidebar-nav.css" rel="stylesheet"> -->
        <!-- <link href="css/custom/media-query.css" rel="stylesheet"> -->
        <!-- endinject -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .payment-msg-container{
                width: 60%;
                height: 70%;
                background: #fff;
                border: 3px solid #d4d4d4;
                border-radius: 3px;
                position: absolute;
                top: 0;
                bottom:0;
                right: 0;
                left: 0;
                margin:auto;
            }
            .payment-header{
                padding: 27px 0;
                background: #000000;
            }
            .payment-header img{
                margin:0 auto;
            }
            .payment-msg{
                padding: 0 30px;
                height: 67%;
                overflow-y: scroll;
                margin: 30px 0;
            }
            .payment-msg div{
                /*overflow-y: scroll;
                height: 100%;*/
            }
            .payment-msg h1{
                color: #0f0f0f;
                font-size: 36px;
                text-transform: uppercase;
                font-weight: 700;
                text-align: center;
                margin-bottom: 20px;
            }
            .payment-msg p{
                font-size: 18px;
                color: #0f0f0f;
                line-height: 36px;
                text-transform: uppercase;
                text-align: center;
                font-weight: 600;
            }
            .payment-msg a{
                background: #f58a1f;
                width: 165px;
                display: block;
                margin: 30px auto 0;
            }
            @media only screen and (max-width : 768px) {
                .payment-msg-container {
                    width: 90%;
                    height: auto;
                    border-radius: 3px;
                    top: 30px;
                    bottom: 30px;
                }
            }
        </style>
    </head>

    <body>
        <div class="payment-msg-container">
            <div class="payment-header">
                <a href="http://www.chrysalis.net.in">
                    <img src="images/logo.png" title="Chrysalis" class="img-responsive">
                </a>
            </div>
            <div class="payment-msg">
                <div>
                    <h1>Thank you</h1>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <p>for registering with Chrysalis,<br>
    we welcome you to this wonderful fraternity.</p>
                    <a class="btn btn-default" href="http://www.chrysalis.net.in">Visit Website</a>
                </div>
            </div>
        </div>
    </body>
</html>