<?php include 'mainHeader.php';?>
<?php include 'mobileNav.php';?>
<div class="container-full header-container inner-header-container">
    <?php include 'header.php';?>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 padding-top-bottom">
                <!-- <h4 class="section-title">Business Offerings</h4>
                <p class="title-qoute col-lg-8 col-md-8 col-sm-12 center-block">
                    Programs For Entrepreneurs And Their Teams
                </p> -->


                <div class="contact_tab myspace">
                    <ul id="myTab" class="container text-center nav nav-tabs" role="tablist">
                        <li class="active" id = "tgob-li"><a href="#tgob" role="tab" data-toggle="tab">Quotes on Life</a></li>
                        <li id = "pankh-li"><a href="#pankh" role="tab" data-toggle="tab">Quotes on Entrepreneurship and Leadership</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="tgob">
                            <div class="program-info">
                                <h4> </h4>  
                                <!--projects-->
                              <!--projects-->
                                <div class="gallery" id="projects">
                                    <div class="container">

                                        <div class="filtr-container">
                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote1.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote1.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote2.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote2.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote3.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote3.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>



                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote1.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote1.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote2.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote2.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote3.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote3.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                             <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote1.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote1.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote2.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote2.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote3.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote3.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="clearfix"> </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div><!-- End Tab Pane -->

                        <div class="tab-pane fade" id="pankh">
                            <div class="program-info">
                                <h4> </h4>  
                                <!--projects-->
                              <!--projects-->
                                <div class="gallery" id="projects">
                                    <div class="container">

                                        <div class="filtr-container">
                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote1.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote1.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote2.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote2.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote3.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote3.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>



                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote1.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote1.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote2.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote2.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote3.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote3.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                             <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote1.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote1.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote2.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote2.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>


                                            <div class=" filtr-item" data-category="3" data-sort="Luminous night">
                                                <a href="images/quote3.jpeg" class="b-link-stripe b-animate-go  thickbox">
                                                    <figure>
                                                        <img src="images/quote3.jpeg" class="img-responsive" alt=" " />  <figcaption>
                                                            <h3>CHRYSALIS</h3>                                                          
                                                        </figcaption>
                                                    </figure>
                                                </a>
                                            </div>

                                            <div class="clearfix"> </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                               
                        </div>
                    </div>

                </div><!-- /end my tab content -->
            </div><!-- /contact_tab -->


        </div>
    </div>
</div>
</div>
<div class="container-full">
    <div class="container">
        <div class="row">


            <div class="container">
                <div class="row">

                    <!-- Form -->
                    <div class="nb-form">
                        <p class="title text-center">Send Enquiry</p>   

                        <form method="POST" role="form" id="myform">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" id="" placeholder="Enter Full Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="" placeholder="Enter Email">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" id="" placeholder="Enter Phone No.">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="inputMessage" class="form-control" rows="3" required="required"></textarea>
                            </div>
                            <center>
                                <input type="submit" name="name" id="submit" value="Submit" class="btn btn-default">
                            </center>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 uprog-lnk">
                <p>To know about our upcoming program <a href="<?php echo $eventPath; ?>events-list/">click here</a></p>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

<script type="text/javascript" src="js/bootstrap.js"></script>

<script src="js/jquery.filterizr.js"></script>

<script type="text/javascript">
    $(function() {
        $('.filtr-container').filterizr();
    });
</script>
<!---->
<script src="js/jquery.chocolat.js"></script>
<!--light-box-files -->
<script>
    $(function() {
        $('.filtr-item a').Chocolat();
    });
</script>  

